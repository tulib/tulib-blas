#ifndef LINALG_VECTOR_HPP_INCLUDED
#define LINALG_VECTOR_HPP_INCLUDED

#include <stdexcept>

#include <tulib/linalg/array_base.hpp>
#include <tulib/util/random.hpp>
#include <tulib/linalg/assert_same_shape_fwd.hpp>
#include <tulib/linalg/lapack_wrapper/lapack_wrapper.hpp>

namespace tulib
{

/**
 * Vector class
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
class vector
   :  public linalg::detail::array_base<T, SIZE_T>
{
   public:
      //!@{
      using base_type = linalg::detail::array_base<T, SIZE_T>;
      using param_type = typename base_type::param_type;
      using real_type = typename base_type::real_type;
      using size_type = typename base_type::size_type;
      //!@}

      //! Default c-tor
      vector() = default;

      //! c-tor
      vector
         (  size_type a_size
         ,  param_type a_val = 0
         )
         :  base_type
               (  a_size
               )
         ,  _size
               (  a_size
               )
      {
         auto size = a_size;
         auto* ptr = this->get_data();
         for(size_type i=0; i<size; ++i)
         {
            ptr[i] = a_val;
         }
      }

      //! Copy c-tor
      vector
         (  const vector& a_other
         )
         :  base_type
               (  a_other.capacity()
               )
         ,  _size
               (  a_other._size
               )
      {
         // Reserve only does something if a_size > _capacity
         this->reserve(this->_size);

         auto* ptr = this->get_data();
         const auto* other_ptr = a_other.get_data();

         for(size_type i=0; i<this->_size; ++i)
         {
            ptr[i] = other_ptr[i];
         }
      }

      //! Move c-tor
      vector
         (  vector&& a_other
         )  noexcept
         :  base_type
               (  std::move(a_other)
               )
         ,  _size
               (  std::move(a_other._size)
               )
      {
      }

      //! Copy assign
      vector& operator=
         (  const vector& a_other
         )
      {
         if (  this != &a_other
            )
         {
            this->_size = a_other._size;
            this->reserve(this->_size);

            auto* ptr = this->get_data();
            const auto* other_ptr = a_other.get_data();

            for(size_type i=0; i<this->_size; ++i)
            {
               ptr[i] = other_ptr[i];
            }
         }

         return *this;
      }

      //! Move assign
      vector& operator=
         (  vector&& a_other
         )  noexcept
      {
         base_type::operator=(std::move(a_other));
         _size = std::move(a_other._size);

         return *this;
      }

      //! Resize
      void resize
         (  size_type a_size
         ,  bool a_save_old = true
         )
      {
         // Reserve only does something if a_size > _capacity
         this->reserve(a_size, a_save_old);
         this->_size = a_size;
      }

      /** @name Element access **/
      //!@{
      param_type& operator()
         (  size_type a_idx
         )  noexcept
      {
         return this->get_data()[a_idx];
      }
      const param_type& operator()
         (  size_type a_idx
         )  const noexcept
      {
         return this->get_data()[a_idx];
      }
      param_type& at
         (  size_type a_idx
         )
      {
         if (  a_idx < this->size()
            )
         {
            return this->get_data()[a_idx];
         }
         else
         {
            throw std::out_of_range("Index out of range!");
         }
      }
      const param_type& at
         (  size_type a_idx
         )  const
      {
         if (  a_idx < this->size()
            )
         {
            return this->get_data()[a_idx];
         }
         else
         {
            throw std::out_of_range("Index out of range!");
         }
      }
      //!@}

      /** @name Math interface **/
      //!@{
      vector& operator+=
         (  const vector& a_other
         )
      {
         assert_same_shape(*this, a_other);
         auto size = this->size();
         auto* ptr = this->get_data();
         const auto* other_ptr = a_other.get_data();
         for(size_type i=0; i<size; ++i)
         {
            ptr[i] += other_ptr[i];
         }
         return *this;
      }

      vector& operator-=
         (  const vector& a_other
         )
      {
         assert_same_shape(*this, a_other);
         auto size = this->size();
         auto* ptr = this->get_data();
         const auto* other_ptr = a_other.get_data();
         for(size_type i=0; i<size; ++i)
         {
            ptr[i] -= other_ptr[i];
         }
         return *this;
      }

      template
         <  typename SCALAR
         >
      vector& scale
         (  const SCALAR& a_scalar
         )
      {
         auto size = this->size();
         auto* ptr = this->get_data();
         for(size_type i=0; i<size; ++i)
         {
            ptr[i] *= a_scalar;
         }
         return *this;
      }

      template
         <  typename SCALAR
         >
      vector& axpy
         (  const SCALAR& a_scalar
         ,  const vector& a_other
         )
      {
         assert_same_shape(*this, a_other);
         int size = (int)this->size();
         T* ptr = this->get_data();
         T* other_ptr = const_cast<vector&>(a_other).get_data();
         auto a = T(a_scalar);
         int inc = 1;

         lapack_wrapper::axpy(&size, &a, other_ptr, &inc, ptr, &inc);

         return *this;
      }
      //!@}

   private:
      //! Size
      size_type _size = 0;

      //! Size impl
      size_type size_impl
         (
         )  const override
      {
         return this->_size;
      }

      //! Clear impl
      void clear_impl
         (
         )  override
      {
         this->_size = 0;
      }
};

} /* namespace tulib */

#include <tulib/linalg/assert_same_shape.hpp>

namespace tulib
{

//! Math operators
template
   <  typename T
   , typename SIZE_T = long
   >
vector<T, SIZE_T> operator+
   (  const vector<T,SIZE_T>& a_left
   ,  const vector<T,SIZE_T>& a_right
   )
{
   auto result = a_left;
   result += a_right;
   return result;
}

template
   <  typename T
   ,  typename SIZE_T = long
   >
vector<T,SIZE_T> operator-
   (  const vector<T,SIZE_T>& a_left
   ,  const vector<T,SIZE_T>& a_right
   )
{
   auto result = a_left;
   result -= a_right;
   return result;
}

/**
 * Output
 *
 * @param a_os
 * @param a_mat
 * @return
 *    a_os
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
std::ostream& operator<<
   (  std::ostream& a_os
   ,  const vector<T, SIZE_T>& a_vec
   )
{
   using size_type = SIZE_T;

   a_os  << "(";
   for(size_type i=0; i<a_vec.size(); ++i)
   {
      a_os  << a_vec(i);
      if (  i < a_vec.size() - 1
         )
      {
         a_os  << ", ";
      }
   }
   a_os  << ")" << std::endl;

   return a_os;
}

//! make random
template
   <  typename T
   ,  typename SIZE_T = long
   >
tulib::vector<T, SIZE_T> make_random_vector
   (  long a_size
   )
{
   tulib::vector<T, SIZE_T> result(a_size);

   for(auto& v : result)
   {
      v = util::rand_signed_float<T>();
   }

   return result;
}

} /* namespace tulib */


#endif /* LINALG_VECTOR_HPP_INCLUDED */
