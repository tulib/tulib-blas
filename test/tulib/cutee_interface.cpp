#include <test/cutee_interface.hpp>

#include <string>
#include <sstream>
#include <regex>

namespace tulib::test
{

void run_suite
   (  cutee::suite& s
   )
{
   std::stringstream sstr;
   std::stringstream fancy_sstr;

#if defined(CUTEE_VERSION)
   cutee::writer_collection wc
      (  std::forward_as_tuple(sstr, cutee::format::raw)
      ,  std::forward_as_tuple(fancy_sstr, cutee::format::fancy)
      );

   s.run(wc);
#else
   s.run(sstr);
#endif

   if (  fancy_sstr.str().empty()
      )
   {
      std::cout   << sstr.str() << std::flush;
   }
   else
   {
      std::cout   << fancy_sstr.str() << std::flush;
   }
}

} /* namespace tulib::test */
