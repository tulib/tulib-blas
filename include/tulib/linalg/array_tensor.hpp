#ifndef LINALG_ARRAY_TENSOR_HPP_INCLUDED
#define LINALG_ARRAY_TENSOR_HPP_INCLUDED

#include <tulib/linalg/tensor_base.hpp>
#include <tulib/linalg/array_base.hpp>

namespace tulib
{

/**
 * Standard (full) tensor.
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
class array_tensor
   :  public linalg::detail::tensor_base<T, SIZE_T>
   ,  public linalg::detail::array_base<T, SIZE_T>
{
};

} /* namespace tulib */


#endif /* LINALG_ARRAY_TENSOR_HPP_INCLUDED */
