#ifndef LAPACK_WRAPPER_LINEAR_SYSTEM_HPP_INCLUDED
#define LAPACK_WRAPPER_LINEAR_SYSTEM_HPP_INCLUDED

#include <tulib/linalg/lapack_wrapper/lapack_wrapper.hpp>
#include <tulib/meta/complex.hpp>
#include <tulib/linalg/lapack_wrapper/tags.hpp>

namespace tulib
{

/**
 * Struct for holding solution to linear equations
 **/
template
   <  typename T
   >
struct lineq_solution
{
   int _n = 0;
   int _nrhs = 0;
   int _info = -1;
   std::unique_ptr<T[]> _x = nullptr;

   //! c-tor
   lineq_solution
      (  int a_n
      ,  int a_nrhs
      ,  int a_info
      ,  T* a_x
      )
      :  _n(a_n)
      ,  _nrhs(a_nrhs)
      ,  _info(a_info)
   {
      auto size = _n*_nrhs;
      this->_x = std::make_unique<T[]>(size);
      for(int i=0; i<size; ++i)
      {
         this->_x[i] = a_x[i];
      }
   }

   //! sanity check
   bool check
      (
      )  const
   {
      return (_info == 0) && _x;
   }
};

/**
 * Load solution into matrix
 *
 * @return
 *    False, if solution has failed
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
bool load_solution
   (  const lineq_solution<T>& a_sol
   ,  matrix<T, SIZE_T>& a_mat
   )
{
   if (  !a_sol.check()
      )
   {
      return false;
   }

   auto n = a_sol._n;
   auto nrhs = a_sol._nrhs;
   a_mat.resize(n,nrhs);

   T* ptr = a_sol._x.get();

   for(int j=0; j<nrhs; ++j)
   {
      for(int i=0; i<n; ++i)
      {
         a_mat(i,j) = *(ptr++);
      }
   }

   return true;
}

/**
 * Load solution into vector
 *
 * @return
 *    False, if solution has failed
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
bool load_solution
   (  const lineq_solution<T>& a_sol
   ,  vector<T, SIZE_T>& a_vec
   )
{
   assert(a_sol._nrhs == 1);

   if (  !a_sol.check()
      )
   {
      return false;
   }

   auto n = a_sol._n;
   a_vec.resize(n);

   T* ptr = a_sol._x.get();

   for(int i=0; i<n; ++i)
   {
      a_vec(i) = *(ptr++);
   }

   return true;
}

/**
 * Solve general linear system: A X = B
 *
 * @param a_matrix      Matrix A in column-major format
 * @param a_n           Dimension of A (assumed square)
 * @param a_rhs         RHS matrix/vector (B)
 * @param a_nrhs        Number of colmuns in B
 * @param a_tag         Tag
 * @return
 *    lineq_solution
 **/
template
   <  typename T
   >
lineq_solution<T> linear_system
   (  T* a_matrix
   ,  size_t a_n
   ,  T* a_rhs
   ,  size_t a_nrhs
   ,  matrix_tag::general_matrix a_tag
   )
{
   // Call gesv
   int n = a_n;
   int nrhs = a_nrhs;
   T* a = a_matrix;
   T* b = a_rhs;
   int lda = std::max(1, n);
   int ldb = std::max(1, n);
   int info;
   auto ipiv = std::make_unique<int[]>(std::max(1,n));
   lapack_wrapper::gesv(&n, &nrhs, a, &lda, ipiv.get(), b, &ldb, &info);

   // Transfer to result
   return lineq_solution<T>(n, nrhs, info, b);
}

/**
 * Solve hemitian linear system: A X = B
 *
 * @param a_matrix      Matrix A in column-major format
 * @param a_n           Dimension of A (assumed square)
 * @param a_rhs         RHS matrix/vector (B)
 * @param a_nrhs        Number of colmuns in B
 * @param a_tag         Tag
 * @param a_uplo        Upper or lower part stored?
 * @return
 *    lineq_solution
 **/
template
   <  typename T
   >
lineq_solution<T> linear_system
   (  T* a_matrix
   ,  size_t a_n
   ,  T* a_rhs
   ,  size_t a_nrhs
   ,  matrix_tag::hermitian_matrix a_tag
   ,  char a_uplo = 'U'
   )
{
   // Call sysv for real numbers
   if constexpr   (  !tulib::meta::is_complex_v<T>
                  )
   {
      return linear_system(a_matrix, a_n, a_rhs, a_nrhs, matrix_tag::symmetric, a_uplo);
   }
   else
   {
      // Call hesv
      char uplo = a_uplo;
      int n = a_n;
      int nrhs = a_nrhs;
      T* a = a_matrix;
      T* b = a_rhs;
      int lda = std::max(1, n);
      int ldb = std::max(1, n);
      int info;
      auto ipiv = std::make_unique<int[]>(std::max(1,n));

      int lwork = -1;
      T work_query;
      // Do workspace query
      lapack_wrapper::hesv(&uplo, &n, &nrhs, nullptr, &lda, nullptr, nullptr, &ldb, &work_query, &lwork, &info);
      if (  info != 0
         )
      {
         TULIB_EXCEPTION("info != 0 after workspace query!");
      }
      lwork = static_cast<int>(std::real(work_query));
      auto work = std::make_unique<T[]>(lwork);

      // Do the work
      lapack_wrapper::hesv(&uplo, &n, &nrhs, a, &lda, ipiv.get(), b, &ldb, work.get(), &lwork, &info);

      // Transfer to result
      return lineq_solution<T>(n, nrhs, info, b);
   }
}

/**
 * Solve symmetric linear system: A X = B
 *
 * @param a_matrix      Matrix A in column-major format
 * @param a_n           Dimension of A (assumed square)
 * @param a_rhs         RHS matrix/vector (B)
 * @param a_nrhs        Number of colmuns in B
 * @param a_tag         Tag
 * @param a_uplo        Upper or lower part stored?
 * @return
 *    lineq_solution
 **/
template
   <  typename T
   >
lineq_solution<T> linear_system
   (  T* a_matrix
   ,  size_t a_n
   ,  T* a_rhs
   ,  size_t a_nrhs
   ,  matrix_tag::symmetric_matrix a_tag
   ,  char a_uplo = 'U'
   )
{
   // Call sysv
   char uplo = a_uplo;
   int n = a_n;
   int nrhs = a_nrhs;
   T* a = a_matrix;
   T* b = a_rhs;
   int lda = std::max(1, n);
   int ldb = std::max(1, n);
   int info;
   auto ipiv = std::make_unique<int[]>(std::max(1,n));

   int lwork = -1;
   T work_query;
   // Do workspace query
   lapack_wrapper::sysv(&uplo, &n, &nrhs, nullptr, &lda, nullptr, nullptr, &ldb, &work_query, &lwork, &info);
   if (  info != 0
      )
   {
      TULIB_EXCEPTION("info != 0 after workspace query!");
   }
   lwork = static_cast<int>(std::real(work_query));
   auto work = std::make_unique<T[]>(lwork);

   // Do the work
   lapack_wrapper::sysv(&uplo, &n, &nrhs, a, &lda, ipiv.get(), b, &ldb, work.get(), &lwork, &info);

   // Transfer to result
   return lineq_solution<T>(n, nrhs, info, b);
}

/**
 * Solve positive definite, Hermitian linear system: A X = B
 *
 * @param a_matrix      Matrix A in column-major format
 * @param a_n           Dimension of A (assumed square)
 * @param a_rhs         RHS matrix/vector (B)
 * @param a_nrhs        Number of colmuns in B
 * @param a_tag         Tag
 * @param a_uplo        Upper or lower part stored?
 * @return
 *    lineq_solution
 **/
template
   <  typename T
   >
lineq_solution<T> linear_system
   (  T* a_matrix
   ,  size_t a_n
   ,  T* a_rhs
   ,  size_t a_nrhs
   ,  matrix_tag::positive_definite_matrix a_tag
   ,  char a_uplo = 'U'
   )
{
   // Call posv
   char uplo = a_uplo;
   int n = a_n;
   int nrhs = a_nrhs;
   T* a = a_matrix;
   T* b = a_rhs;
   int lda = std::max(1, n);
   int ldb = std::max(1, n);
   int info;
   lapack_wrapper::posv(&uplo, &n, &nrhs, a, &lda, b, &ldb, &info);

   // Transfer to result
   return lineq_solution<T>(n, nrhs, info, b);
}

/**
 * Solve general linear system: A X = B
 * Without specifying matrix tag, we assume general.
 *
 * @param a_matrix      Matrix A in column-major format
 * @param a_n           Dimension of A (assumed square)
 * @param a_rhs         RHS matrix/vector (B)
 * @param a_nrhs        Number of colmuns in B
 * @param a_tag         Tag
 * @return
 *    lineq_solution
 **/
template
   <  typename T
   >
lineq_solution<T> linear_system
   (  T* a_matrix
   ,  size_t a_n
   ,  T* a_rhs
   ,  size_t a_nrhs
   )
{
   return linear_system(a_matrix, a_n, a_rhs, a_nrhs, matrix_tag::general);
}

/**
 * Solve general linear system: A X = B
 *
 * @param a_matrix      Matrix A
 * @param a_rhs         RHS matrix (B)
 * @param a_tag         Tag
 * @return
 *    lineq_solution
 **/
template
   <  typename T
   ,  typename TAG_T
   ,  typename SIZE_T = long
   >
lineq_solution<T> linear_system
   (  const matrix<T, SIZE_T>& a_matrix
   ,  const matrix<T, SIZE_T>& b_matrix
   ,  TAG_T a_tag
   )
{
   assert(a_matrix.is_square());
   assert(b_matrix.nrow() == a_matrix.nrow());
   auto a_copy = a_matrix;
   auto b_copy = b_matrix;
   return linear_system(a_copy.get_data(), a_matrix.nrow(), b_copy.get_data(), b_matrix.ncol(), a_tag);
}

/**
 * Solve general linear system: A X = B
 *
 * @param a_matrix      Matrix A
 * @param a_rhs         RHS matrix (B)
 * @return
 *    lineq_solution
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
lineq_solution<T> linear_system
   (  const matrix<T, SIZE_T>& a_matrix
   ,  const matrix<T, SIZE_T>& b_matrix
   )
{
   assert(a_matrix.is_square());
   assert(b_matrix.nrow() == a_matrix.nrow());
   auto a_copy = a_matrix;
   auto b_copy = b_matrix;
   return linear_system(a_copy.get_data(), a_matrix.nrow(), b_copy.get_data(), b_matrix.ncol(), matrix_tag::general);
}

/**
 * Solve general linear system: A x = b
 *
 * @param a_matrix      Matrix A
 * @param a_rhs         RHS vector (B)
 * @param a_tag         Tag
 * @return
 *    lineq_solution
 **/
template
   <  typename T
   ,  typename TAG_T
   ,  typename SIZE_T = long
   >
lineq_solution<T> linear_system
   (  const matrix<T, SIZE_T>& a_matrix
   ,  const vector<T, SIZE_T>& b_vector
   ,  TAG_T a_tag
   )
{
   assert(a_matrix.is_square());
   assert(b_vector.size() == a_matrix.nrow());
   auto a_copy = a_matrix;
   auto b_copy = b_vector;
   return linear_system(a_copy.get_data(), a_matrix.nrow(), b_copy.get_data(), 1, a_tag);
}

/**
 * Solve general linear system: A x = b
 *
 * @param a_matrix      Matrix A
 * @param a_rhs         RHS vector (B)
 * @return
 *    lineq_solution
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
lineq_solution<T> linear_system
   (  const matrix<T, SIZE_T>& a_matrix
   ,  const vector<T, SIZE_T>& b_vector
   )
{
   assert(a_matrix.is_square());
   assert(b_vector.size() == a_matrix.nrow());
   auto a_copy = a_matrix;
   auto b_copy = b_vector;
   return linear_system(a_copy.get_data(), a_matrix.nrow(), b_copy.get_data(), 1, matrix_tag::general);
}

} /* namespace tulib */

#endif /* LAPACK_WRAPPER_LINEAR_SYSTEM_HPP_INCLUDED */
