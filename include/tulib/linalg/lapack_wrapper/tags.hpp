#ifndef LAPACK_WRAPPER_TAGS_HPP_INCLUDED
#define LAPACK_WRAPPER_TAGS_HPP_INCLUDED

namespace tulib
{

/**
 * Struct defining matrix types
 **/
struct matrix_tag
{
   //! General matrix tag
   inline static struct general_matrix                    {} general;

   //! Symmetric matrix tags
   inline static struct symmetric_matrix                  {} symmetric;
   inline static struct packed_symmetric_matrix           {} packed_symmetric;

   //! Hermitian matrix tags
   inline static struct hermitian_matrix                  {} hermitian;
   inline static struct packed_hermitian_matrix           {} packed_hermitian;

   //! Positive definite (+ symmetric/Hermitian) matrix tags
   inline static struct positive_definite_matrix          {} positive_definite;
   inline static struct packed_positive_definite_matrix   {} packed_positive_definite;
};

} /* namespace tulib */

#endif /* LAPACK_WRAPPER_TAGS_HPP_INCLUDED */
