#ifndef LAPACK_WRAPPER_BLAS_INTERFACE_HPP_INCLUDED
#define LAPACK_WRAPPER_BLAS_INTERFACE_HPP_INCLUDED

#include <cassert>

#include <tulib/linalg/lapack_wrapper/lapack_wrapper.hpp>
#include <tulib/linalg/lapack_wrapper/tags.hpp>
#include <tulib/linalg/linalg_fwd.hpp>

namespace tulib
{

/**
 * Wrapper for GEMV
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
void matrix_vector_multiply
   (  const matrix<T, SIZE_T>& a_matrix
   ,  const vector<T, SIZE_T>& a_vector
   ,  vector<T, SIZE_T>& a_result
   ,  matrix_tag::general_matrix a_tag
   ,  char a_trans = 'N'
   ,  T a_alpha = 1
   ,  T a_beta = 0
   )
{
   assert(a_matrix.ncol() == a_vector.size());
   assert(a_result.size() == a_matrix.nrow());

   int m = a_matrix.nrow();
   int n = a_matrix.ncol();
   int lda = std::max(1, m);
   int incx = 1;
   int incy = 1;

   lapack_wrapper::gemv(&a_trans, &m, &n, &a_alpha, const_cast<matrix<T,SIZE_T>&>(a_matrix).get_data(), &lda, const_cast<vector<T,SIZE_T>&>(a_vector).get_data(), &incx, &a_beta, a_result.get_data(), &incy);
}

/**
 * Wrapper for SYMV
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
void matrix_vector_multiply
   (  const matrix<T, SIZE_T>& a_matrix
   ,  const vector<T, SIZE_T>& a_vector
   ,  vector<T, SIZE_T>& a_result
   ,  matrix_tag::symmetric_matrix a_tag
   ,  char a_uplo = 'U'
   ,  T a_alpha = 1
   ,  T a_beta = 0
   )
{
   assert(a_matrix.is_square() && a_matrix.ncol() == a_vector.size());
   assert(a_result.size() == a_matrix.nrow());

   int n = a_matrix.nrow();
   int lda = std::max(1, n);
   int incx = 1;
   int incy = 1;

   lapack_wrapper::symv(&a_uplo, &n, &a_alpha, const_cast<matrix<T,SIZE_T>&>(a_matrix).get_data(), &lda, const_cast<vector<T,SIZE_T>&>(a_vector).get_data(), &incx, &a_beta, a_result.get_data(), &incy);
}

/**
 * Wrapper for HEMV
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
void matrix_vector_multiply
   (  const matrix<T, SIZE_T>& a_matrix
   ,  const vector<T, SIZE_T>& a_vector
   ,  vector<T, SIZE_T>& a_result
   ,  matrix_tag::hermitian_matrix a_tag
   ,  char a_uplo = 'U'
   ,  T a_alpha = 1
   ,  T a_beta = 0
   )
{
   assert(a_matrix.is_square() && a_matrix.ncol() == a_vector.size());
   assert(a_result.size() == a_matrix.nrow());

   int n = a_matrix.nrow();
   int lda = std::max(1, n);
   int incx = 1;
   int incy = 1;

   if constexpr   (  meta::is_complex_v<T>
                  )
   {
      lapack_wrapper::hemv(&a_uplo, &n, &a_alpha, const_cast<matrix<T,SIZE_T>&>(a_matrix).get_data(), &lda, const_cast<vector<T,SIZE_T>&>(a_vector).get_data(), &incx, &a_beta, a_result.get_data(), &incy);
   }
   else
   {
      lapack_wrapper::symv(&a_uplo, &n, &a_alpha, const_cast<matrix<T,SIZE_T>&>(a_matrix).get_data(), &lda, const_cast<vector<T,SIZE_T>&>(a_vector).get_data(), &incx, &a_beta, a_result.get_data(), &incy);
   }
}

/**
 * Wrapper for GEMM
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
void matrix_matrix_multiply
   (  const matrix<T, SIZE_T>& a_matrix
   ,  const matrix<T, SIZE_T>& b_matrix
   ,  matrix<T, SIZE_T>& a_result
   ,  matrix_tag::general_matrix a_tag
   ,  char a_trans = 'N'
   ,  char b_trans = 'N'
   ,  T a_alpha = 1
   ,  T a_beta = 0
   )
{
   assert(a_matrix.ncol() == b_matrix.nrow());
   assert(a_result.nrow() == a_matrix.nrow() && a_result.ncol() == b_matrix.ncol());

   int m = a_matrix.nrow();
   int n = b_matrix.ncol();
   int k = a_matrix.ncol();
   int lda = a_trans == 'N' ? std::max(1, m) : std::max(1, k);
   int ldb = b_trans == 'N' ? std::max(1, k) : std::max(1, n);
   int ldc = std::max(1, m);

   lapack_wrapper::gemm(&a_trans, &b_trans, &m, &n, &k, &a_alpha, const_cast<matrix<T,SIZE_T>&>(a_matrix).get_data(), &lda, const_cast<matrix<T,SIZE_T>&>(b_matrix).get_data(), &ldb, &a_beta, a_result.get_data(), &ldc);
}

/**
 * Wrapper for SYMM
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
void matrix_matrix_multiply
   (  const matrix<T, SIZE_T>& a_matrix
   ,  const matrix<T, SIZE_T>& b_matrix
   ,  matrix<T, SIZE_T>& a_result
   ,  matrix_tag::symmetric_matrix a_tag
   ,  char a_side = 'L'
   ,  char a_uplo = 'U'
   ,  T a_alpha = 1
   ,  T a_beta = 0
   )
{
   assert(a_matrix.is_square() && a_matrix.ncol() == b_matrix.nrow());
   assert(a_result.nrow() == a_matrix.nrow() && a_result.ncol() == b_matrix.ncol());

   int m = a_matrix.nrow();
   int n = b_matrix.ncol();
   int lda = a_side == 'L' ? std::max(1, m) : std::max(1, n);
   int ldb = std::max(1, m);
   int ldc = std::max(1, m);

   lapack_wrapper::symm(&a_side, &a_uplo, &m, &n, &a_alpha, const_cast<matrix<T,SIZE_T>&>(a_matrix).get_data(), &lda, const_cast<matrix<T,SIZE_T>&>(b_matrix).get_data(), &ldb, &a_beta, a_result.get_data(), &ldc);
}

/**
 * Wrapper for HEMM
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
void matrix_matrix_multiply
   (  const matrix<T, SIZE_T>& a_matrix
   ,  const matrix<T, SIZE_T>& b_matrix
   ,  matrix<T, SIZE_T>& a_result
   ,  matrix_tag::hermitian_matrix a_tag
   ,  char a_side = 'L'
   ,  char a_uplo = 'U'
   ,  T a_alpha = 1
   ,  T a_beta = 0
   )
{
   assert(a_matrix.is_square() && a_matrix.ncol() == b_matrix.nrow());
   assert(a_result.nrow() == a_matrix.nrow() && a_result.ncol() == b_matrix.ncol());

   int m = a_matrix.nrow();
   int n = b_matrix.ncol();
   int lda = a_side == 'L' ? std::max(1, m) : std::max(1, n);
   int ldb = std::max(1, m);
   int ldc = std::max(1, m);

   if constexpr   (  meta::is_complex_v<T>
                  )
   {
      lapack_wrapper::hemm(&a_side, &a_uplo, &m, &n, &a_alpha, const_cast<matrix<T,SIZE_T>&>(a_matrix).get_data(), &lda, const_cast<matrix<T,SIZE_T>&>(b_matrix).get_data(), &ldb, &a_beta, a_result.get_data(), &ldc);
   }
   else
   {
      lapack_wrapper::symm(&a_side, &a_uplo, &m, &n, &a_alpha, const_cast<matrix<T,SIZE_T>&>(a_matrix).get_data(), &lda, const_cast<matrix<T,SIZE_T>&>(b_matrix).get_data(), &ldb, &a_beta, a_result.get_data(), &ldc);
   }
}

//! Matrix-vector multiplication
template
   <  typename T
   ,  typename SIZE_T = long
   >
vector<T, SIZE_T> operator*
   (  const matrix<T, SIZE_T>& a_mat
   ,  const vector<T, SIZE_T>& a_vec
   )
{
   vector<T, SIZE_T> result(a_mat.nrow());

   matrix_vector_multiply(a_mat, a_vec, result, matrix_tag::general);

   return result;
}

//! Matrix-matrix multiplication
template
   <  typename T
   ,  typename SIZE_T = long
   >
matrix<T, SIZE_T> operator*
   (  const matrix<T, SIZE_T>& a_left
   ,  const matrix<T, SIZE_T>& a_right
   )
{
   matrix<T, SIZE_T> result(a_left.nrow(), a_right.ncol());

   matrix_matrix_multiply(a_left, a_right, result, matrix_tag::general);

   return result;
}

} /* namespace tulib */

#endif /* LAPACK_WRAPPER_BLAS_INTERFACE_HPP_INCLUDED */
