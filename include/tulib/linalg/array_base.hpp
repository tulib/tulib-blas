#ifndef ARRAY_BASE_HPP_INCLUDED
#define ARRAY_BASE_HPP_INCLUDED

#include <memory>
#include <numeric>
#include <limits>

#include <tulib/meta/complex.hpp>
#include <tulib/math/math.hpp>
#include <tulib/math/range_operations.hpp>

namespace tulib
{
namespace linalg::detail
{

/**
 * Base class for arrays (vector, matrix, tensor)
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
class array_base
{
   public:
      static_assert(std::numeric_limits<SIZE_T>::is_integer, "size_type must be integer type");

      /** @name Alias **/
      //!@{
      using param_type = T;
      using real_type = tulib::meta::real_type_t<param_type>;
      using size_type = SIZE_T;
      using data_type = std::unique_ptr<param_type[]>;
      using iterator = param_type*;
      using const_iterator = const param_type*;
      //!@}
      
      //! c-tor
      array_base
         (  size_type a_capacity = 0
         )
         :  _data
               (  this->allocate_new(a_capacity)
               )
         ,  _capacity
               (  a_capacity
               )
      {
      }

      //! Move c-tor
      array_base
         (  array_base&& a_other
         )  noexcept
         :  _data
               (  std::move(a_other._data)
               )
         ,  _capacity
               (  std::move(a_other._capacity)
               )
      {
      }

      //! Move assign
      array_base& operator=
         (  array_base&& a_other
         )  noexcept
      {
         _data = std::move(a_other._data);
         _capacity = std::move(a_other._capacity);

         return *this;
      }

      //! Virtual destructor
      virtual ~array_base() = default;

      //! Size
      size_type size
         (
         )  const
      {
         return this->size_impl();
      }

      //! Capacity
      size_type capacity
         (
         )  const
      {
         return this->_capacity;
      }

      //! Reserve
      void reserve
         (  size_type a_capacity
         ,  bool a_save_old = true
         )
      {
         if (  a_capacity > this->_capacity
            )
         {
            auto new_data = this->allocate_new(a_capacity);

            if (  a_save_old
               )
            {
               auto cur_size = this->size();
               for(size_type i=0; i<cur_size; ++i)
               {
                  new_data[i] = std::move(this->_data[i]);
               }
            }

            std::swap(new_data, this->_data);
         }
      }

      //! Clear
      void clear
         (
         )
      {
         this->_capacity = 0;
         this->_data = nullptr;
         this->clear_impl();
      }

      //! Squared norm
      real_type norm2
         (
         )  const
      {
      return std::transform_reduce
#ifdef TULIB_PAR_STL
         (  std::execution::par_unseq
         ,  this->begin(), this->end()
#else
         (  this->begin(), this->end()
#endif /* TULIB_PAR_STL */
         ,  real_type(0.0)
         ,  std::plus<>()
         ,  [](const T& i_)
            {
               return tulib::math::abs2(i_);
            }
         );
      }

      //! Norm
      real_type norm
         (
         )  const
      {
         return std::sqrt(this->norm2());
      }

      //! Conjugate
      void conj
         (
         )
      {
         if constexpr   (  tulib::meta::is_complex_v<param_type>
                        )
         {
            auto size = this->size();
            for(size_type i=0; i<size; ++i)
            {
               this->_data[i] = tulib::math::conj(this->_data[i]);
            }
         }
      }

      //! Get data (const)
      const param_type* get_data
         (
         )  const
      {
         return this->_data.get();
      }

      //! Get data
      param_type* get_data
         (
         )
      {
         return this->_data.get();
      }

      /** @name Iterator interface **/
      //!@{
      const_iterator begin
         (
         )  const
      {
         return this->_data.get();
      }

      const_iterator end
         (
         )  const
      {
         return this->_data.get() + this->size();
      }
      //!@}

      //! Non-const iterator begin
      iterator begin
         (
         )
      {
         return this->_data.get();
      }

      //! Non-const iterator end
      iterator end
         (
         )
      {
         return this->_data.get() + this->size();
      }


   private:
      //! Data
      data_type _data = nullptr;

      //! Capacity (allocated size)
      size_type _capacity = 0;

      //! Size
      virtual size_type size_impl
         (
         )  const = 0;

      //! Clear
      virtual void clear_impl
         (
         )  = 0;

      //! Allocate new memory
      data_type allocate_new
         (  size_type a_capacity
         )  const
      {
         return std::make_unique<param_type[]>(a_capacity);
      }
};

} /* namespace linalg::detail */
} /* namespace tulib */


#include <tulib/linalg/lapack_wrapper/lapack_wrapper.hpp>

namespace tulib
{
/**
 * Dot product
 *
 * @param a_left
 * @param a_right
 * @return
 *    <a_left|a_right>
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
typename linalg::detail::array_base<T, SIZE_T>::param_type dot_product
   (  const linalg::detail::array_base<T, SIZE_T>& left_
   ,  const linalg::detail::array_base<T, SIZE_T>& right_
   )
{
   using param_type = typename linalg::detail::array_base<T, SIZE_T>::param_type;
   using size_type = typename linalg::detail::array_base<T, SIZE_T>::size_type;

   int size = (int)left_.size();
   assert(size == (int)right_.size());

   if constexpr ( tulib::meta::is_complex_v<T> )
   {
      return std::transform_reduce
#ifdef TULIB_PAR_STL
         (  std::execution::par_unseq
         ,  left_.begin(), left_.end()
#else
         (  left_.begin(), left_.end()
#endif /* TULIB_PAR_STL */
         ,  right_.begin()
         ,  T(0.0)
         ,  std::plus<>()
         ,  [](const T& i_, const T& j_)
         {
            return tulib::math::conj(i_) * j_;
         }
         );
   }
   else
   {
      int inc = 1;
      auto& l = const_cast<linalg::detail::array_base<T, SIZE_T>&>(left_);
      auto& r = const_cast<linalg::detail::array_base<T, SIZE_T>&>(right_);
      return lapack_wrapper::dot(&size, l.get_data(), &inc, r.get_data(), &inc);
   }
}

} /* namespace tulib */

#endif /* ARRAY_BASE_HPP_INCLUDED */
