#include <test/util_test.hpp>

namespace tulib::test
{

void util_test
   (
   )
{
   cutee::suite s("util_test_suite");

   // Add tests
   s.add_test<function_wrapper_test>("function-wrapper test");
   s.add_test<input_definitions_test>("input-definitions test");
   s.add_test<io_test>("io test");
   
   run_suite(s);
}

} /*namespace tulib::test */
