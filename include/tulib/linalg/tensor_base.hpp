#ifndef LINALG_TENSOR_BASE_HPP_INCLUDED
#define LINALG_TENSOR_BASE_HPP_INCLUDED

#include <vector>

namespace tulib::linalg::detail
{

/**
 * Base class for tensors
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
class tensor_base
{
   public:
      //! Alias
      using param_type = T;
      using real_type = tulib::meta::real_type_t<param_type>;
      using size_type = SIZE_T;
      using dims_type = std::vector<size_type>;

      //! Get dimensions
      const dims_type& dims
         (
         )  const
      {
         return this->_dims;
      }

   private:
      //! Dimensions
      dims_type _dims;
};

} /* namespace tulib::linalg::detail */


#endif /* LINALG_TENSOR_BASE_HPP_INCLUDED */
