#include <complex>

#include <tulib/linalg/lapack_wrapper/lapack_wrapper.hpp>

/**
 * Interface to LAPACK
 **/
extern "C"
{
   /**
    * DOT
    **/
   //!@{
   float sdot_(int* n, float* dx, int* incx, float* dy, int* incy);
   double ddot_(int* n, double* dx, int* incx, double* dy, int* incy);
//   void cdotcsub_(int* n, std::complex<float>* dx, int* incx, std::complex<float>* dy, int* incy, std::complex<float>* res);
//   void zdotcsub_(int* n, std::complex<double>* dx, int* incx, std::complex<double>* dy, int* incy, std::complex<double>* res);
   //!@}

   /**
    * AXPY
    **/
   //!@{
   void saxpy_(int* n, float* da, float* dx, int* incx, float* dy, int* incy);
   void daxpy_(int* n, double* da, double* dx, int* incx, double* dy, int* incy);
   void caxpy_(int* n, std::complex<float>* da, std::complex<float>* dx, int* incx, std::complex<float>* dy, int* incy);
   void zaxpy_(int* n, std::complex<double>* da, std::complex<double>* dx, int* incx, std::complex<double>* dy, int* incy);
   //!@}

   /** 
    * GEMV
    **/
   //!@{
   void sgemv_(char* trans, int* m, int* n, float* alpha, float* a, int* lda, float* x, int* incx, float* beta, float* y, int* incy);
   void dgemv_(char* trans, int* m, int* n, double* alpha, double* a, int* lda, double* x, int* incx, double* beta, double* y, int* incy);
   void cgemv_(char* trans, int* m, int* n, std::complex<float>* alpha, std::complex<float>* a, int* lda, std::complex<float>* x, int* incx, std::complex<float>* beta, std::complex<float>* y, int* incy);
   void zgemv_(char* trans, int* m, int* n, std::complex<double>* alpha, std::complex<double>* a, int* lda, std::complex<double>* x, int* incx, std::complex<double>* beta, std::complex<double>* y, int* incy);
   //!@}
   
   /**
    * SYMV
    **/
   //!@{
   void ssymv_(char* uplo, int* n, float*                  alpha, float*                 a, int* lda, float*                 x, int* incx, float*                beta, float*                  y, int* incy);
   void dsymv_(char* uplo, int* n, double*                 alpha, double*                a, int* lda, double*                x, int* incx, double*               beta, double*                 y, int* incy);
   void csymv_(char* uplo, int* n, std::complex<float>*    alpha, std::complex<float>*   a, int* lda, std::complex<float>*   x, int* incx, std::complex<float>*  beta, std::complex<float>*    y, int* incy);
   void zsymv_(char* uplo, int* n, std::complex<double>*   alpha, std::complex<double>*  a, int* lda, std::complex<double>*  x, int* incx, std::complex<double>* beta, std::complex<double>*   y, int* incy);
   //!@}
   
   /**
    * HEMV
    **/
   //!@{
   void chemv_(char* uplo, int* n, std::complex<float>*    alpha, std::complex<float>*   a, int* lda, std::complex<float>*   x, int* incx, std::complex<float>*  beta, std::complex<float>*    y, int* incy);
   void zhemv_(char* uplo, int* n, std::complex<double>*   alpha, std::complex<double>*  a, int* lda, std::complex<double>*  x, int* incx, std::complex<double>* beta, std::complex<double>*   y, int* incy);
   //!@}


   /** 
    * GEMM
    **/
   //!@{
   void sgemm_(char* transa, char* transb, int* m, int* n, int* k, float* alpha, float* a, int* lda, float* b, int* ldb, float* beta, float* c, int* ldc);
   void dgemm_(char* transa, char* transb, int* m, int* n, int* k, double* alpha, double* a, int* lda, double* b, int* ldb, double* beta, double* c, int* ldc);
   void cgemm_(char* transa, char* transb, int* m, int* n, int* k, std::complex<float>* alpha, std::complex<float>* a, int* lda, std::complex<float>* b, int* ldb, std::complex<float>* beta, std::complex<float>* c, int* ldc);
   void zgemm_(char* transa, char* transb, int* m, int* n, int* k, std::complex<double>* alpha, std::complex<double>* a, int* lda, std::complex<double>* b, int* ldb, std::complex<double>* beta, std::complex<double>* c, int* ldc);
   //!@}

   /**
    * SYMM
    **/
   //!@{
   void ssymm_(char* side, char* uplo, int* m, int* n, float*                alpha, float*                 a, int* lda, float*                 b, int* ldb, float*                 beta, float*                  c, int* ldc);
   void dsymm_(char* side, char* uplo, int* m, int* n, double*               alpha, double*                a, int* lda, double*                b, int* ldb, double*                beta, double*                 c, int* ldc);
   void csymm_(char* side, char* uplo, int* m, int* n, std::complex<float>*  alpha, std::complex<float>*   a, int* lda, std::complex<float>*   b, int* ldb, std::complex<float>*   beta, std::complex<float>*    c, int* ldc);
   void zsymm_(char* side, char* uplo, int* m, int* n, std::complex<double>* alpha, std::complex<double>*  a, int* lda, std::complex<double>*  b, int* ldb, std::complex<double>*  beta, std::complex<double>*   c, int* ldc);
   //!@}
   
   /**
    * HEMM
    **/
   //!@{
   void chemm_(char* side, char* uplo, int* m, int* n, std::complex<float>*  alpha, std::complex<float>*   a, int* lda, std::complex<float>*   b, int* ldb, std::complex<float>*   beta, std::complex<float>*    c, int* ldc);
   void zhemm_(char* side, char* uplo, int* m, int* n, std::complex<double>* alpha, std::complex<double>*  a, int* lda, std::complex<double>*  b, int* ldb, std::complex<double>*  beta, std::complex<double>*   c, int* ldc);
   //!@}

   /**
    * GESV
    **/
   //!@{
   void sgesv_(int* n, int* nrhs, float* a, int* lda, int* ipiv, float* b, int* ldb, int* info);
   void dgesv_(int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb, int* info);
   void cgesv_(int* n, int* nrhs, std::complex<float>* a, int* lda, int* ipiv, std::complex<float>* b, int* ldb, int* info);
   void zgesv_(int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, int* info);
   //!@}
 
   /**
    * SYSV
    **/
   //!@{
   void ssysv_(char* uplo, int* n, int* nrhs, float* a, int* lda, int* ipiv, float* b, int* ldb, float* work, int* lwork, int* info);
   void dsysv_(char* uplo, int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb, double* work, int* lwork, int* info);
   void csysv_(char* uplo, int* n, int* nrhs, std::complex<float>* a, int* lda, int* ipiv, std::complex<float>* b, int* ldb, std::complex<float>* work, int* lwork, int* info);
   void zsysv_(char* uplo, int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, std::complex<double>* work, int* lwork, int* info);
   //!@}

   /**
    * HESV
    **/
   //!@{
   void chesv_(char* uplo, int* n, int* nrhs, std::complex<float>* a, int* lda, int* ipiv, std::complex<float>* b, int* ldb, std::complex<float>* work, int* lwork, int* info);
   void zhesv_(char* uplo, int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, std::complex<double>* work, int* lwork, int* info);
   //!@}

   /**
    * POSV
    **/
   //!@{
   void sposv_(char* uplo, int* n, int* nrhs, float* a, int* lda, float* b, int* ldb, int* info);
   void dposv_(char* uplo, int* n, int* nrhs, double* a, int* lda, double* b, int* ldb, int* info);
   void cposv_(char* uplo, int* n, int* nrhs, std::complex<float>* a, int* lda, std::complex<float>* b, int* ldb, int* info);
   void zposv_(char* uplo, int* n, int* nrhs, std::complex<double>* a, int* lda, std::complex<double>* b, int* ldb, int* info);
   //!@}

   /**
    * GESVD
    **/
   //!@{
   void sgesvd_(char* jobu, char* jobvt, int* m, int* n, float* a, int* lda, float* s, float* u, int* ldu, float* vt, int* ldvt, float* work, int* lwork, int* info);
   void dgesvd_(char* jobu, char* jobvt, int* m, int* n, double* a, int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt, double* work, int* lwork, int* info);
   void cgesvd_(char* jobu, char* jobvt, int* m, int* n, std::complex<float>* a, int* lda, float* s, std::complex<float>* u, int* ldu, std::complex<float>* vt, int* ldvt, std::complex<float>* work, int* lwork, float* rwork, int* info);
   void zgesvd_(char* jobu, char* jobvt, int* m, int* n, std::complex<double>* a, int* lda, double* s, std::complex<double>* u, int* ldu, std::complex<double>* vt, int* ldvt, std::complex<double>* work, int* lwork, double* rwork, int* info);
   //!@}
   
   /**
    * GESDD
    **/
   //!@{
   void sgesdd_(char* jobz, int* m, int* n, float* a, int* lda, float* s, float* u, int* ldu, float* vt, int* ldvt, float* work, int* lwork, int* iwork, int* info);
   void dgesdd_(char* jobz, int* m, int* n, double* a, int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt, double* work, int* lwork, int* iwork, int* info);
   void cgesdd_(char* jobz, int* m, int* n, std::complex<float>* a, int* lda, float* s, std::complex<float>* u, int* ldu, std::complex<float>* vt, int* ldvt, std::complex<float>* work, int* lwork, float* rwork, int* iwork, int* info);
   void zgesdd_(char* jobz, int* m, int* n, std::complex<double>* a, int* lda, double* s, std::complex<double>* u, int* ldu, std::complex<double>* vt, int* ldvt, std::complex<double>* work, int* lwork, double* rwork, int* iwork, int* info);
   //!@}
}

namespace tulib::lapack_wrapper
{
/**
 * DOT
 **/
//!@{
float dot(int* n, float* dx, int* incx, float* dy, int* incy)
{
   return sdot_(n, dx, incx, dy, incy);
}
double dot(int* n, double* dx, int* incx, double* dy, int* incy)
{
   return ddot_(n, dx, incx, dy, incy);
}
//!@}

/**
 * AXPY
 **/
//!@{
void axpy(int* n, float* da, float* dx, int* incx, float* dy, int* incy)
{
   saxpy_(n, da, dx, incx, dy, incy);
   return;
}
void axpy(int* n, double* da, double* dx, int* incx, double* dy, int* incy)
{
   daxpy_(n, da, dx, incx, dy, incy);
   return;
}
void axpy(int* n, std::complex<float>* da, std::complex<float>* dx, int* incx, std::complex<float>* dy, int* incy)
{
   caxpy_(n, da, dx, incx, dy, incy);
   return;
}
void axpy(int* n, std::complex<double>* da, std::complex<double>* dx, int* incx, std::complex<double>* dy, int* incy)
{
   zaxpy_(n, da, dx, incx, dy, incy);
   return;
}
//!@}

/**
 * GEMV
 **/
//!@{
void gemv(char* trans, int* m, int* n, float* alpha, float* a, int* lda, float* x, int* incx, float* beta, float* y, int* incy)
{
   sgemv_(trans, m, n, alpha, a, lda, x, incx, beta, y, incy);
   return;
}

void gemv(char* trans, int* m, int* n, double* alpha, double* a, int* lda, double* x, int* incx, double* beta, double* y, int* incy)
{
   dgemv_(trans, m, n, alpha, a, lda, x, incx, beta, y, incy);
   return;
}

void gemv(char* trans,  int* m, int* n, std::complex<float>* alpha, std::complex<float>* a, int* lda, std::complex<float>* x, int* incx, std::complex<float>* beta, std::complex<float>* y, int* incy)
{
   cgemv_(trans,  m, n,  alpha, a, lda, x, incx, beta, y, incy);
   return;
}

void gemv(char* trans,  int* m, int* n, std::complex<double>* alpha, std::complex<double>* a, int* lda, std::complex<double>* x, int* incx, std::complex<double>* beta, std::complex<double>* y, int* incy)
{
   zgemv_(trans,  m, n,  alpha, a, lda, x, incx, beta, y, incy);
   return;
}
//!@}

/**
 * SYMV
 **/
//!@{
void symv(char* uplo, int* n, float*                  alpha, float*                 a, int* lda, float*                 x, int* incx, float*                beta, float*                  y, int* incy)
{
   ssymv_(uplo, n, alpha, a, lda, x, incx, beta, y, incy);
   return;
}
void symv(char* uplo, int* n, double*                 alpha, double*                a, int* lda, double*                x, int* incx, double*               beta, double*                 y, int* incy)
{
   dsymv_(uplo, n, alpha, a, lda, x, incx, beta, y, incy);
   return;
}
void symv(char* uplo, int* n, std::complex<float>*    alpha, std::complex<float>*   a, int* lda, std::complex<float>*   x, int* incx, std::complex<float>*  beta, std::complex<float>*    y, int* incy)
{
   csymv_(uplo, n, alpha, a, lda, x, incx, beta, y, incy);
   return;
}
void symv(char* uplo, int* n, std::complex<double>*   alpha, std::complex<double>*  a, int* lda, std::complex<double>*  x, int* incx, std::complex<double>* beta, std::complex<double>*   y, int* incy)
{
   zsymv_(uplo, n, alpha, a, lda, x, incx, beta, y, incy);
   return;
}
//!@}

/**
 * HEMV
 **/
//!@{
void hemv(char* uplo, int* n, std::complex<float>*    alpha, std::complex<float>*   a, int* lda, std::complex<float>*   x, int* incx, std::complex<float>*  beta, std::complex<float>*    y, int* incy)
{
   chemv_(uplo, n, alpha, a, lda, x, incx, beta, y, incy);
   return;
}
void hemv(char* uplo, int* n, std::complex<double>*   alpha, std::complex<double>*  a, int* lda, std::complex<double>*  x, int* incx, std::complex<double>* beta, std::complex<double>*   y, int* incy)
{
   zhemv_(uplo, n, alpha, a, lda, x, incx, beta, y, incy);
   return;
}
//!@}

/**
 * GEMM
 **/
//!@{
void gemm(char* transa, char* transb, int* m, int* n, int* k, float* alpha, float* a, int* lda, float* b, int* ldb, float* beta, float* c, int* ldc)
{
   sgemm_(transa, transb, m, n, k, alpha, a, lda, b, ldb, beta, c, ldc);
   return;
}

void gemm(char* transa, char* transb, int* m, int* n, int* k, double* alpha, double* a, int* lda, double* b, int* ldb, double* beta, double* c, int* ldc)
{
   dgemm_(transa, transb, m, n, k, alpha, a, lda, b, ldb, beta, c, ldc);
   return;
}

void gemm(char* transa, char* transb, int* m, int* n, int* k, std::complex<float>* alpha, std::complex<float>* a, int* lda, std::complex<float>* b, int* ldb, std::complex<float>* beta, std::complex<float>* c, int* ldc)
{
   cgemm_(transa, transb, m, n, k, alpha, a, lda, b, ldb, beta, c, ldc);
   return;
}

void gemm(char* transa, char* transb, int* m, int* n, int* k, std::complex<double>* alpha, std::complex<double>* a, int* lda, std::complex<double>* b, int* ldb, std::complex<double>* beta, std::complex<double>* c, int* ldc)
{
   zgemm_(transa, transb, m, n, k, alpha, a, lda, b, ldb, beta, c, ldc);
   return;
}
//!@}

/**
 * SYMM
 **/
//!@{
void symm(char* side, char* uplo, int* m, int* n, float*                alpha, float*                 a, int* lda, float*                 b, int* ldb, float*                 beta, float*                  c, int* ldc)
{
   ssymm_(side, uplo, m, n, alpha, a, lda, b, ldb, beta, c, ldc);
   return;
}
void symm(char* side, char* uplo, int* m, int* n, double*               alpha, double*                a, int* lda, double*                b, int* ldb, double*                beta, double*                 c, int* ldc)
{
   dsymm_(side, uplo, m, n, alpha, a, lda, b, ldb, beta, c, ldc);
   return;
}
void symm(char* side, char* uplo, int* m, int* n, std::complex<float>*  alpha, std::complex<float>*   a, int* lda, std::complex<float>*   b, int* ldb, std::complex<float>*   beta, std::complex<float>*    c, int* ldc)
{
   csymm_(side, uplo, m, n, alpha, a, lda, b, ldb, beta, c, ldc);
   return;
}
void symm(char* side, char* uplo, int* m, int* n, std::complex<double>* alpha, std::complex<double>*  a, int* lda, std::complex<double>*  b, int* ldb, std::complex<double>*  beta, std::complex<double>*   c, int* ldc)
{
   zsymm_(side, uplo, m, n, alpha, a, lda, b, ldb, beta, c, ldc);
   return;
}
//!@}

/**
 * HEMM
 **/
//!@{
void hemm(char* side, char* uplo, int* m, int* n, std::complex<float>*  alpha, std::complex<float>*   a, int* lda, std::complex<float>*   b, int* ldb, std::complex<float>*   beta, std::complex<float>*    c, int* ldc)
{
   chemm_(side, uplo, m, n, alpha, a, lda, b, ldb, beta, c, ldc);
   return;
}
void hemm(char* side, char* uplo, int* m, int* n, std::complex<double>* alpha, std::complex<double>*  a, int* lda, std::complex<double>*  b, int* ldb, std::complex<double>*  beta, std::complex<double>*   c, int* ldc)
{
   zhemm_(side, uplo, m, n, alpha, a, lda, b, ldb, beta, c, ldc);
   return;
}
//!@}

/**
 * GESV
 **/
//!@{
void gesv(int* n, int* nrhs, float* a, int* lda, int* ipiv, float* b, int* ldb, int* info)
{
   sgesv_(n, nrhs, a, lda, ipiv, b, ldb, info);
   return;
}
void gesv(int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb, int* info)
{
   dgesv_(n, nrhs, a, lda, ipiv, b, ldb, info);
   return;
}
void gesv(int* n, int* nrhs, std::complex<float>* a, int* lda, int* ipiv, std::complex<float>* b, int* ldb, int* info)
{
   cgesv_(n, nrhs, a, lda, ipiv, b, ldb, info);
   return;
}
void gesv(int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, int* info)
{
   zgesv_(n, nrhs, a, lda, ipiv, b, ldb, info);
   return;
}
//!@}

/**
 * SYSV
 **/
//!@{
void sysv(char* uplo, int* n, int* nrhs, float* a, int* lda, int* ipiv, float* b, int* ldb, float* work, int* lwork, int* info)
{
   ssysv_(uplo, n, nrhs, a, lda, ipiv, b, ldb, work, lwork, info);
   return;
}
void sysv(char* uplo, int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb, double* work, int* lwork, int* info)
{
   dsysv_(uplo, n, nrhs, a, lda, ipiv, b, ldb, work, lwork, info);
   return;
}
void sysv(char* uplo, int* n, int* nrhs, std::complex<float>* a, int* lda, int* ipiv, std::complex<float>* b, int* ldb, std::complex<float>* work, int* lwork, int* info)
{
   csysv_(uplo, n, nrhs, a, lda, ipiv, b, ldb, work, lwork, info);
   return;
}
void sysv(char* uplo, int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, std::complex<double>* work, int* lwork, int* info)
{
   zsysv_(uplo, n, nrhs, a, lda, ipiv, b, ldb, work, lwork, info);
   return;
}
//!@}

/**
 * HESV
 **/
//!@{
void hesv(char* uplo, int* n, int* nrhs, std::complex<float>* a, int* lda, int* ipiv, std::complex<float>* b, int* ldb, std::complex<float>* work, int* lwork, int* info)
{
   chesv_(uplo, n, nrhs, a, lda, ipiv, b, ldb, work, lwork, info);
   return;
}
void hesv(char* uplo, int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, std::complex<double>* work, int* lwork, int* info)
{
   zhesv_(uplo, n, nrhs, a, lda, ipiv, b, ldb, work, lwork, info);
   return;
}
//!@}

/**
 * POSV
 **/
//!@{
void posv(char* uplo, int* n, int* nrhs, float* a, int* lda, float* b, int* ldb, int* info)
{
   sposv_(uplo, n, nrhs, a, lda, b, ldb, info);
   return;
}
void posv(char* uplo, int* n, int* nrhs, double* a, int* lda, double* b, int* ldb, int* info)
{
   dposv_(uplo, n, nrhs, a, lda, b, ldb, info);
   return;
}
void posv(char* uplo, int* n, int* nrhs, std::complex<float>* a, int* lda, std::complex<float>* b, int* ldb, int* info)
{
   cposv_(uplo, n, nrhs, a, lda, b, ldb, info);
   return;
}
void posv(char* uplo, int* n, int* nrhs, std::complex<double>* a, int* lda, std::complex<double>* b, int* ldb, int* info)
{
   zposv_(uplo, n, nrhs, a, lda, b, ldb, info);
   return;
}
//!@}

/**
 * GESVD
 **/
//!@{
void gesvd(char* jobu, char* jobvt, int* m, int* n, float* a, int* lda, float* s, float* u, int* ldu, float* vt, int* ldvt, float* work, int* lwork, int* info)
{
   sgesvd_(jobu, jobvt, m, n, a, lda, s, u, ldu, vt, ldvt, work, lwork, info);
   return;
}
void gesvd(char* jobu, char* jobvt, int* m, int* n, double* a, int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt, double* work, int* lwork, int* info)
{
   dgesvd_(jobu, jobvt, m, n, a, lda, s, u, ldu, vt, ldvt, work, lwork, info);
   return;
}
void gesvd(char* jobu, char* jobvt, int* m, int* n, std::complex<float>* a, int* lda, float* s, std::complex<float>* u, int* ldu, std::complex<float>* vt, int* ldvt, std::complex<float>* work, int* lwork, float* rwork, int* info)
{
   cgesvd_(jobu, jobvt, m, n, a, lda, s, u, ldu, vt, ldvt, work, lwork, rwork, info);
   return;
}
void gesvd(char* jobu, char* jobvt, int* m, int* n, std::complex<double>* a, int* lda, double* s, std::complex<double>* u, int* ldu, std::complex<double>* vt, int* ldvt, std::complex<double>* work, int* lwork, double* rwork, int* info)
{
   zgesvd_(jobu, jobvt, m, n, a, lda, s, u, ldu, vt, ldvt, work, lwork, rwork, info);
   return;
}
//!@}

/**
 * GESDD
 **/
//!@{
void gesdd(char* jobz, int* m, int* n, float* a, int* lda, float* s, float* u, int* ldu, float* vt, int* ldvt, float* work, int* lwork, int* iwork, int* info)
{
   sgesdd_(jobz, m, n, a, lda, s, u, ldu, vt, ldvt, work, lwork, iwork, info);
   return;
}
void gesdd(char* jobz, int* m, int* n, double* a, int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt, double* work, int* lwork, int* iwork, int* info)
{
   dgesdd_(jobz, m, n, a, lda, s, u, ldu, vt, ldvt, work, lwork, iwork, info);
   return;
}
void gesdd(char* jobz, int* m, int* n, std::complex<float>* a, int* lda, float* s, std::complex<float>* u, int* ldu, std::complex<float>* vt, int* ldvt, std::complex<float>* work, int* lwork, float* rwork, int* iwork, int* info)
{
   cgesdd_(jobz, m, n, a, lda, s, u, ldu, vt, ldvt, work, lwork, rwork, iwork, info);
   return;
}
void gesdd(char* jobz, int* m, int* n, std::complex<double>* a, int* lda, double* s, std::complex<double>* u, int* ldu, std::complex<double>* vt, int* ldvt, std::complex<double>* work, int* lwork, double* rwork, int* iwork, int* info)
{
   zgesdd_(jobz, m, n, a, lda, s, u, ldu, vt, ldvt, work, lwork, rwork, iwork, info);
   return;
}
//!@}

} /* tulib::lapack_wrapper */
