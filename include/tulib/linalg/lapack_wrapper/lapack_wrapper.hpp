#ifndef LAPACK_WRAPPER_WRAPPER_HPP_INCLUDED
#define LAPACK_WRAPPER_WRAPPER_HPP_INCLUDED

#include <complex>

namespace tulib::lapack_wrapper
{

/** @name BLAS **/
//!@{

/**
 * DOT
 **/
//!@{
float dot(int* n, float* dx, int* incx, float* dy, int* incy);
double dot(int* n, double* dx, int* incx, double* dy, int* incy);
//!@}

/**
 * AXPY
 **/
//!@{
void axpy(int* n, float* da, float* dx, int* incx, float* dy, int* incy);
void axpy(int* n, double* da, double* dx, int* incx, double* dy, int* incy);
void axpy(int* n, std::complex<float>* da, std::complex<float>* dx, int* incx, std::complex<float>* dy, int* incy);
void axpy(int* n, std::complex<double>* da, std::complex<double>* dx, int* incx, std::complex<double>* dy, int* incy);
//!@}

/**
 * GEMV
 **/
//!@{
void gemv(char* trans, int* m, int* n, float*                  alpha, float*                 a, int* lda, float*                 x, int* incx, float*                beta, float*                  y, int* incy);
void gemv(char* trans, int* m, int* n, double*                 alpha, double*                a, int* lda, double*                x, int* incx, double*               beta, double*                 y, int* incy);
void gemv(char* trans, int* m, int* n, std::complex<float>*    alpha, std::complex<float>*   a, int* lda, std::complex<float>*   x, int* incx, std::complex<float>*  beta, std::complex<float>*    y, int* incy);
void gemv(char* trans, int* m, int* n, std::complex<double>*   alpha, std::complex<double>*  a, int* lda, std::complex<double>*  x, int* incx, std::complex<double>* beta, std::complex<double>*   y, int* incy);
//!@}

/**
 * SYMV
 **/
//!@{
void symv(char* uplo, int* n, float*                  alpha, float*                 a, int* lda, float*                 x, int* incx, float*                beta, float*                  y, int* incy);
void symv(char* uplo, int* n, double*                 alpha, double*                a, int* lda, double*                x, int* incx, double*               beta, double*                 y, int* incy);
void symv(char* uplo, int* n, std::complex<float>*    alpha, std::complex<float>*   a, int* lda, std::complex<float>*   x, int* incx, std::complex<float>*  beta, std::complex<float>*    y, int* incy);
void symv(char* uplo, int* n, std::complex<double>*   alpha, std::complex<double>*  a, int* lda, std::complex<double>*  x, int* incx, std::complex<double>* beta, std::complex<double>*   y, int* incy);
//!@}

/**
 * HEMV
 **/
//!@{
void hemv(char* uplo, int* n, std::complex<float>*    alpha, std::complex<float>*   a, int* lda, std::complex<float>*   x, int* incx, std::complex<float>*  beta, std::complex<float>*    y, int* incy);
void hemv(char* uplo, int* n, std::complex<double>*   alpha, std::complex<double>*  a, int* lda, std::complex<double>*  x, int* incx, std::complex<double>* beta, std::complex<double>*   y, int* incy);
//!@}

/**
 * GEMM
 **/
//!@{
void gemm(char* transa, char* transb, int* m, int* n, int* k, float*                alpha, float*                 a, int* lda, float*                 b, int* ldb, float*                 beta, float*                  c, int* ldc);
void gemm(char* transa, char* transb, int* m, int* n, int* k, double*               alpha, double*                a, int* lda, double*                b, int* ldb, double*                beta, double*                 c, int* ldc);
void gemm(char* transa, char* transb, int* m, int* n, int* k, std::complex<float>*  alpha, std::complex<float>*   a, int* lda, std::complex<float>*   b, int* ldb, std::complex<float>*   beta, std::complex<float>*    c, int* ldc);
void gemm(char* transa, char* transb, int* m, int* n, int* k, std::complex<double>* alpha, std::complex<double>*  a, int* lda, std::complex<double>*  b, int* ldb, std::complex<double>*  beta, std::complex<double>*   c, int* ldc);
//!@}

/**
 * SYMM
 **/
//!@{
void symm(char* side, char* uplo, int* m, int* n, float*                alpha, float*                 a, int* lda, float*                 b, int* ldb, float*                 beta, float*                  c, int* ldc);
void symm(char* side, char* uplo, int* m, int* n, double*               alpha, double*                a, int* lda, double*                b, int* ldb, double*                beta, double*                 c, int* ldc);
void symm(char* side, char* uplo, int* m, int* n, std::complex<float>*  alpha, std::complex<float>*   a, int* lda, std::complex<float>*   b, int* ldb, std::complex<float>*   beta, std::complex<float>*    c, int* ldc);
void symm(char* side, char* uplo, int* m, int* n, std::complex<double>* alpha, std::complex<double>*  a, int* lda, std::complex<double>*  b, int* ldb, std::complex<double>*  beta, std::complex<double>*   c, int* ldc);
//!@}

/**
 * HEMM
 **/
//!@{
void hemm(char* side, char* uplo, int* m, int* n, std::complex<float>*  alpha, std::complex<float>*   a, int* lda, std::complex<float>*   b, int* ldb, std::complex<float>*   beta, std::complex<float>*    c, int* ldc);
void hemm(char* side, char* uplo, int* m, int* n, std::complex<double>* alpha, std::complex<double>*  a, int* lda, std::complex<double>*  b, int* ldb, std::complex<double>*  beta, std::complex<double>*   c, int* ldc);
//!@}

//!@}
/** BLAS END **/

/** @name Linear-equation solvers **/
//!@{

/**
 * GESV
 **/
//!@{
void gesv(int* n, int* nrhs, float* a, int* lda, int* ipiv, float* b, int* ldb, int* info);
void gesv(int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb, int* info);
void gesv(int* n, int* nrhs, std::complex<float>* a, int* lda, int* ipiv, std::complex<float>* b, int* ldb, int* info);
void gesv(int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, int* info);
//!@}

/**
 * SYSV
 **/
//!@{
void sysv(char* uplo, int* n, int* nrhs, float* a, int* lda, int* ipiv, float* b, int* ldb, float* work, int* lwork, int* info);
void sysv(char* uplo, int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb, double* work, int* lwork, int* info);
void sysv(char* uplo, int* n, int* nrhs, std::complex<float>* a, int* lda, int* ipiv, std::complex<float>* b, int* ldb, std::complex<float>* work, int* lwork, int* info);
void sysv(char* uplo, int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, std::complex<double>* work, int* lwork, int* info);
//!@}

/**
 * HESV
 **/
//!@{
void hesv(char* uplo, int* n, int* nrhs, std::complex<float>* a, int* lda, int* ipiv, std::complex<float>* b, int* ldb, std::complex<float>* work, int* lwork, int* info);
void hesv(char* uplo, int* n, int* nrhs, std::complex<double>* a, int* lda, int* ipiv, std::complex<double>* b, int* ldb, std::complex<double>* work, int* lwork, int* info);
//!@}

/**
 * POSV
 **/
//!@{
void posv(char* uplo, int* n, int* nrhs, float* a, int* lda, float* b, int* ldb, int* info);
void posv(char* uplo, int* n, int* nrhs, double* a, int* lda, double* b, int* ldb, int* info);
void posv(char* uplo, int* n, int* nrhs, std::complex<float>* a, int* lda, std::complex<float>* b, int* ldb, int* info);
void posv(char* uplo, int* n, int* nrhs, std::complex<double>* a, int* lda, std::complex<double>* b, int* ldb, int* info);
//!@}

//¡@}
/** Linear-equation solvers END **/

/** @name SVD BEGIN **/
//!@{

/**
 * GESVD
 **/
//!@{
void gesvd(char* jobu, char* jobvt, int* m, int* n, float* a, int* lda, float* s, float* u, int* ldu, float* vt, int* ldvt, float* work, int* lwork, int* info);
void gesvd(char* jobu, char* jobvt, int* m, int* n, double* a, int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt, double* work, int* lwork, int* info);
void gesvd(char* jobu, char* jobvt, int* m, int* n, std::complex<float>* a, int* lda, float* s, std::complex<float>* u, int* ldu, std::complex<float>* vt, int* ldvt, std::complex<float>* work, int* lwork, float* rwork, int* info);
void gesvd(char* jobu, char* jobvt, int* m, int* n, std::complex<double>* a, int* lda, double* s, std::complex<double>* u, int* ldu, std::complex<double>* vt, int* ldvt, std::complex<double>* work, int* lwork, double* rwork, int* info);
//!@}

/**
 * GESDD
 **/
//!@{
void gesdd(char* jobz, int* m, int* n, float* a, int* lda, float* s, float* u, int* ldu, float* vt, int* ldvt, float* work, int* lwork, int* iwork, int* info);
void gesdd(char* jobz, int* m, int* n, double* a, int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt, double* work, int* lwork, int* iwork, int* info);
void gesdd(char* jobz, int* m, int* n, std::complex<float>* a, int* lda, float* s, std::complex<float>* u, int* ldu, std::complex<float>* vt, int* ldvt, std::complex<float>* work, int* lwork, float* rwork, int* iwork, int* info);
void gesdd(char* jobz, int* m, int* n, std::complex<double>* a, int* lda, double* s, std::complex<double>* u, int* ldu, std::complex<double>* vt, int* ldvt, std::complex<double>* work, int* lwork, double* rwork, int* iwork, int* info);
//!@}

//!@}

} /* namespace tulib::lapack_wrapper */

#endif /* LAPACK_WRAPPER_WRAPPER_HPP_INCLUDED */
