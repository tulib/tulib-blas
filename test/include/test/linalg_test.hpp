#ifndef TULIB_LINALG_TEST_HPP_INCLUDED
#define TULIB_LINALG_TEST_HPP_INCLUDED

#include <test/cutee_interface.hpp>

#include <tulib/vector.hpp>
#include <tulib/matrix.hpp>
#include <tulib/math.hpp>
#include <tulib/lapack_wrapper.hpp>

namespace tulib::test
{

/**
 *
 **/
template
   <  typename T
   >
struct blas_test
   :  public cutee::test
{
   public:
      void run
         (
         )  override
      {
         static_assert(std::is_floating_point_v<T> || tulib::meta::is_complex_v<T>);
         using real_t = tulib::meta::real_type_t<T>;
         size_t size = 4;

         tulib::vector<T> vec(size, 1.0);
         UNIT_ASSERT_FEQUAL(tulib::math::wrap_norm2(vec), real_t(size), "Vector of 1.0 norm2 failed!");
         tulib::matrix<T> mat(size, size, 1.0);
         UNIT_ASSERT_FEQUAL(tulib::math::wrap_norm2(mat), real_t(size*size), "Matrix of 1.0 norm2 failed!");

         auto mv = mat * vec;
         auto dot = tulib::math::wrap_dot_product(vec, mv);
         UNIT_ASSERT_FEQUAL(std::real(dot), real_t(size*size), "Real part of v^conj*M*v of 1.0 failed!");
         UNIT_ASSERT_FZERO(std::imag(dot), std::real(dot), "Imag part of v^conj*M*v of 1.0 failed!");
      }
};

/**
 *
 **/
template
   <  typename T
   >
struct linear_system_test
   :  public cutee::test
{
   public:
      void run
         (
         )  override
      {
         static_assert(std::is_floating_point_v<T> || tulib::meta::is_complex_v<T>);
         using real_t = tulib::meta::real_type_t<T>;
         const size_t n = 10;
         const size_t nrhs = 5;

         const auto bvec = make_random_vector<T>(n);
         const auto bvec_norm = tulib::math::wrap_norm(bvec);
         const auto bmat = make_random_matrix<T>(n, nrhs);
         const auto bmat_norm = tulib::math::wrap_norm(bmat);

         // Solve with general matrix - both with 1 and multiple RHSs
         {
            const auto a = make_random_matrix<T>(n, n);
            unsigned cn = condition_number(a);

            auto sol_v = linear_system(a, bvec, matrix_tag::general);
            if (  !sol_v.check()
               )
            {
               std::cerr   << " linear_solution for GESV with 1 rhs. info = " << sol_v._info << std::endl;
            }
            tulib::vector<T> x_v;
            UNIT_ASSERT(load_solution(sol_v, x_v), "load_solution failed for GESV to vector");
            auto check_v = a * x_v;
            check_v -= bvec;
            auto diffnorm_v = tulib::math::wrap_norm(check_v);
            auto err_v = std::min(diffnorm_v, diffnorm_v/bvec_norm);
            UNIT_ASSERT_FZERO_PREC(err_v, real_t(1.0), cn, "GESV with vector RHS failed!");

            auto sol_m = linear_system(a, bmat, matrix_tag::general);
            if (  !sol_m.check()
               )
            {
               std::cerr   << " linear_solution for GESV with multiple rhs. info = " << sol_m._info << std::endl;
            }
            tulib::matrix<T> x_m;
            UNIT_ASSERT(load_solution(sol_m, x_m), "load_solution failed for GESV to matrix");
            auto check_m = a * x_m;
            check_m -= bmat;
            auto diffnorm_m = tulib::math::wrap_norm(check_m);
            auto err_m = std::min(diffnorm_m, diffnorm_m/bmat_norm);
            UNIT_ASSERT_FZERO_PREC(err_m, real_t(1.0), cn, "GESV with matrix RHS failed!");
         }

         // Solve with hermitian matrix - both with 1 and multiple RHSs
         {
            const auto a = make_random_hermitian_matrix<T>(n);
            unsigned cn = condition_number(a);

            auto sol_v = linear_system(a, bvec, matrix_tag::hermitian);
            if (  !sol_v.check()
               )
            {
               std::cerr   << " linear_solution for HESV/SYSV with 1 rhs. info = " << sol_v._info << std::endl;
            }
            tulib::vector<T> x_v;
            UNIT_ASSERT(load_solution(sol_v, x_v), "load_solution failed for HESV to vector");
            auto check_v = a * x_v;
            check_v -= bvec;
            auto diffnorm_v = tulib::math::wrap_norm(check_v);
            auto err_v = std::min(diffnorm_v, diffnorm_v/bvec_norm);
            UNIT_ASSERT_FZERO_PREC(err_v, real_t(1.0), cn, "SYSV/HESV with vector RHS failed!");

            auto sol_m = linear_system(a, bmat, matrix_tag::hermitian);
            if (  !sol_m.check()
               )
            {
               std::cerr   << " linear_solution for HESV/SYSV with multiple rhs. info = " << sol_m._info << std::endl;
            }
            tulib::matrix<T> x_m;
            UNIT_ASSERT(load_solution(sol_m, x_m), "load_solution failed for HESV to matrix");
            auto check_m = a * x_m;
            check_m -= bmat;
            auto diffnorm_m = tulib::math::wrap_norm(check_m);
            auto err_m = std::min(diffnorm_m, diffnorm_m/bmat_norm);
            UNIT_ASSERT_FZERO_PREC(err_m, real_t(1.0), cn, "SYSV/HESV with matrix RHS failed!");
         }

         // Solve with hermitian, positive definite matrix - both with 1 and multiple RHSs
         {
            // Make positive-definite matrix
            tulib::matrix<T> a(n,n);
            std::vector<tulib::vector<T>> vv(n);
            for(auto& v : vv)
            {
               v = make_random_vector<T>(n);
            }
            for(size_t i=0; i<n; ++i)
            {
               a(i,i) += vv[i].norm2();
               for(size_t j=i+1; j<n; ++j)
               {
                  auto dot = dot_product(vv[i], vv[j]);
                  a(i,j) += dot;
                  a(j,i) += tulib::math::conj(dot);
               }
            }
            unsigned cn = condition_number(a);

            auto sol_v = linear_system(a, bvec, matrix_tag::positive_definite);
            if (  !sol_v.check()
               )
            {
               std::cerr   << " linear_solution for POSV with 1 rhs. info = " << sol_v._info << std::endl;
            }
            tulib::vector<T> x_v;
            UNIT_ASSERT(load_solution(sol_v, x_v), "load_solution failed for POSV to vector");
            auto check_v = a * x_v;
            check_v -= bvec;
            auto diffnorm_v = tulib::math::wrap_norm(check_v);
            auto err_v = std::min(diffnorm_v, diffnorm_v/bvec_norm);
            UNIT_ASSERT_FZERO_PREC(err_v, real_t(1.0), cn, "POSV with vector RHS failed!");

            auto sol_m = linear_system(a, bmat, matrix_tag::positive_definite);
            if (  !sol_m.check()
               )
            {
               std::cerr   << " linear_solution for POSV with multiple rhss. info = " << sol_m._info << std::endl;
            }
            tulib::matrix<T> x_m;
            UNIT_ASSERT(load_solution(sol_m, x_m), "load_solution failed for POSV to matrix");
            auto check_m = a * x_m;
            check_m -= bmat;
            auto diffnorm_m = tulib::math::wrap_norm(check_m);
            auto err_m = std::min(diffnorm_m, diffnorm_m/bmat_norm);
            UNIT_ASSERT_FZERO_PREC(err_m, real_t(1.0), cn, "POSV with matrix RHS failed!");
         }
      }
};

} /* namespace tulib::test */

#endif /* TULIB_LINALG_TEST_HPP_INCLUDED */
