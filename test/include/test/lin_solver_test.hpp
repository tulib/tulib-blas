#ifndef TULIB_LIN_SOLVER_TEST_HPP_INCLUDED
#define TULIB_LIN_SOLVER_TEST_HPP_INCLUDED

#include <test/cutee_interface.hpp>

#include <tulib/vector.hpp>
#include <tulib/matrix.hpp>
#include <tulib/it_solver/lin_solver/linear_equation_solver.hpp>
#include <tulib/linalg/lapack_wrapper/linear_system.hpp>

namespace tulib::test
{

/**
 * 
 **/
template
   <  typename T
   >
struct conjugate_gradient_test
   :  public cutee::test
{
   void run() override
   {
      using vec_t = tulib::vector<T>;
      using mat_t = tulib::matrix<T>;

      // Initial test
      vec_t v0(2);
      v0(0) = T(1.0);
      v0(1) = T(2.0);
      mat_t m0(2,2);
      m0(0,0) = T(4.0);
      m0(1,0) = T(1.0);
      m0(0,1) = T(1.0);
      m0(1,1) = T(3.0);

      tulib::lin_solver::settings_type settings; 
      settings.add(lin_solver::inputKey::SOLVER, lin_solver::solverType::CG);
      settings.add(lin_solver::inputKey::CONVCHECK, lin_solver::convType::RELATIVERESNORM);
      settings.add(lin_solver::inputKey::MAXITER, 20);
      settings.validate(lin_solver::get_lin_solver_defaults());

      tulib::linear_equation_solver sol0
         (  [&](const vec_t& v_) -> vec_t
            {
               return m0*v_;
            }
         ,  lin_solver::solverType::CG
         ,  1.e-6
         ,  10
         );
      auto [x0, conv0] = sol0.solve(v0);
      UNIT_ASSERT(conv0, "Failed to solve 2x2 problem");

      // Also solve using dense methods
      auto dense_x0 = tulib::linear_system(m0, v0);
      UNIT_ASSERT_FEQUAL_PREC(x0(0), dense_x0._x[0], 100, "2x2 system wrong 0th element");
      UNIT_ASSERT_FEQUAL_PREC(x0(1), dense_x0._x[1], 100, "2x2 system wrong 1st element");

      constexpr size_t size = 50;
      // Generate random matrix and construct A^dagger A (Hermitian, positive definite)
      const auto A = tulib::make_random_positive_definite_matrix<T>(size);
      auto a = tulib::make_random_vector<T>(size);

      // Solve with tulib::vector as vec_t
      tulib::linear_equation_solver sol([&A](const vec_t& v_) -> vec_t { return A*v_; }, settings);
      sol.solve(a);

      // Generate another random matrix the same way
      const auto B = tulib::make_random_positive_definite_matrix<T>(size);
      auto b = tulib::make_random_vector<T>(size);
      const auto rhs2 = std::make_pair(std::move(a), std::move(b));

      // Solve using pair of vectors, one for each matrix, i.e. the system (A, 0; 0, B) (x; y) = (a; b)   <=> Ax=a and By=b
      tulib::linear_equation_solver sol2([&A,&B](const std::pair<vec_t, vec_t>& v_) -> std::pair<vec_t, vec_t> { return std::make_pair(A*v_.first, B*v_.second); }, settings);
      sol2.solve(rhs2);
   }
};

/**
 * 
 **/
template
   <  typename T
   >
struct diag_precon_conjugate_gradient_test
   :  public cutee::test
{
   void run() override
   {
      using real_t = tulib::meta::real_type_t<T>;
      using vec_t = tulib::vector<T>;
      using mat_t = tulib::matrix<T>;
      constexpr size_t size = 50;

      // Generate random matrix and construct A^dagger A (Hermitian, positive definite)
      const auto A = tulib::make_random_positive_definite_matrix<T>(size);
      auto a = tulib::make_random_vector<T>(size);

      tulib::lin_solver::settings_type settings; 
      settings.add(lin_solver::inputKey::SOLVER, lin_solver::solverType::CG);
      settings.add(lin_solver::inputKey::CONVCHECK, lin_solver::convType::RELATIVERESNORM);
      settings.add(lin_solver::inputKey::MAXITER, 20);
      settings.validate(lin_solver::get_lin_solver_defaults());

      struct transformer
      {
         using vec_t = tulib::vector<T>;

         const tulib::matrix<T>& _mat;

         transformer(const tulib::matrix<T>& mat_) : _mat(mat_) {}

         vec_t operator()(const vec_t& v_) const { return _mat * v_; }

         void precondition(vec_t& v_) const
         {
            const auto len = v_.size();
            for(int i=0; i<len; ++i)
            {
               v_(i) /= (std::abs(_mat(i,i)) + T(100*std::numeric_limits<real_t>::epsilon()));
            }
         }

      } trf(A);

      // Solve with tulib::vector as vec_t
      tulib::linear_equation_solver sol(trf, settings);
      sol.solve(a);
   }
};

/**
 * 
 **/
template
   <  typename T
   >
struct conjugate_residual_test
   :  public cutee::test
{
   void run() override
   {
      using vec_t = tulib::vector<T>;
      using mat_t = tulib::matrix<T>;

      // Initial test
      vec_t v0(2);
      v0(0) = T(1.0);
      v0(1) = T(2.0);
      mat_t m0(2,2);
      m0(0,0) = T(4.0);
      m0(1,0) = T(1.0);
      m0(0,1) = T(1.0);
      m0(1,1) = T(3.0);

      tulib::lin_solver::settings_type settings; 
      settings.add(lin_solver::inputKey::SOLVER, lin_solver::solverType::CR);
      settings.add(lin_solver::inputKey::CONVCHECK, lin_solver::convType::RELATIVERESNORM);
      settings.add(lin_solver::inputKey::MAXITER, 20);
      settings.validate(lin_solver::get_lin_solver_defaults());

      tulib::linear_equation_solver sol0
         (  [&](const vec_t& v_) -> vec_t
            {
               return m0*v_;
            }
         ,  settings
         );
      auto [x0, conv0] = sol0.solve(v0);
      UNIT_ASSERT(conv0, "Failed to solve 2x2 problem");

      // Also solve using dense methods
      auto dense_x0 = tulib::linear_system(m0, v0);
      UNIT_ASSERT_FEQUAL_PREC(x0(0), dense_x0._x[0], 100, "2x2 system wrong 0th element");
      UNIT_ASSERT_FEQUAL_PREC(x0(1), dense_x0._x[1], 100, "2x2 system wrong 1st element");

      constexpr size_t size = 50;
      // Generate random Hermitian matrix
      const auto A = tulib::make_random_hermitian_matrix<T>(size);
      auto a = tulib::make_random_vector<T>(size);

      // Solve with tulib::vector as vec_t
      tulib::linear_equation_solver sol([&A](const vec_t& v_) -> vec_t { return A*v_; }, settings);
      sol.solve(a);
   }
};

/**
 * 
 **/
template
   <  typename T
   >
struct diag_precon_conjugate_residual_test
   :  public cutee::test
{
   void run() override
   {
      using real_t = tulib::meta::real_type_t<T>;
      using vec_t = tulib::vector<T>;
      using mat_t = tulib::matrix<T>;
      constexpr size_t size = 50;

      // Generate random Hermitian matrix with small off-diagonal elements
      const auto A = tulib::make_random_hermitian_matrix<T>(size, std::function<T(long, long)>([](long i_, long j_) -> T { return std::exp(-std::abs(i_-j_)); }));
      auto a = tulib::make_random_vector<T>(size);

//      std::cout << "Apply preconditioned CR algorithm with T=" << typeid(T).name() << " to matrix:\n" << A << "\nAnd vector:\n" << a << std::endl;

      tulib::lin_solver::settings_type settings; 
      settings.add(lin_solver::inputKey::SOLVER, lin_solver::solverType::CR);
      settings.add(lin_solver::inputKey::CONVCHECK, lin_solver::convType::RELATIVERESNORM);
      settings.add(lin_solver::inputKey::MAXITER, 20);
      settings.add(lin_solver::inputKey::PRECONDITIONER, lin_solver::preconType::USER_DEFINED);
      settings.validate(lin_solver::get_lin_solver_defaults());

      struct transformer
      {
         using vec_t = tulib::vector<T>;

         const tulib::matrix<T>& _mat;

         transformer(const tulib::matrix<T>& mat_) : _mat(mat_) {}

         vec_t operator()(const vec_t& v_) const { return _mat * v_; }

         void precondition(vec_t& v_) const
         {
            const auto len = v_.size();
            for(int i=0; i<len; ++i)
            {
               v_(i) /= (std::abs(_mat(i,i)) + T(100*std::numeric_limits<real_t>::epsilon()));
            }
         }

      } trf(A);

      // Solve with tulib::vector as vec_t
      tulib::linear_equation_solver sol(trf, settings);
      sol.solve(a);
   }
};
} /* namespace tulib::test */

#endif /* TULIB_LIN_SOLVER_TEST_HPP_INCLUDED */