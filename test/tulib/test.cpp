#include <iostream>

#include <test/driver_wrapper.hpp>

int main
   (  int argc
   ,  char* argv[]
   )
{
   // Initialize drivers
   tulib::test::driver_wrapper drvs;

   // Select drivers (if no args, run all)
   for(int i=1; i<argc; ++i)
   {
      drvs.select_driver(std::string(argv[i]));
   }

   // Run all
   for(const auto& d : drvs.get_drivers())
   {
      d();
   }

   return 0;
}
