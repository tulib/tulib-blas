#include <test/meta_test.hpp>

namespace tulib::test
{

void meta_test
   (
   )
{
   cutee::suite s("meta_test_suite");

   // Add tests
   s.add_test<template_argument_test>("template-argument test");
   s.add_test<function_traits_test>("function_traits test");
   
   run_suite(s);
}

} /*namespace tulib::test */

