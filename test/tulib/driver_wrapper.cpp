#include <cctype>

#include <test/driver_wrapper.hpp>


namespace tulib::test
{

// Forward declare drivers
void linalg_test();
void meta_test();
void math_test();
void util_test();
void lin_solver_test();


// Map of drivers
const std::map<driver_wrapper::key_t, driver_wrapper::driver_t>& driver_wrapper::available_drivers
   (
   )  const
{
   static std::map<key_t, driver_t> drvs =
   {  { "linalg", linalg_test }
   ,  { "meta", meta_test }
   ,  { "math", math_test }
   ,  { "util", util_test }
   ,  { "lin_solver", lin_solver_test }
   };

   return drvs;
}

// Get drivers
const std::vector<typename driver_wrapper::driver_t> driver_wrapper::get_drivers
   (
   )  const
{
   const auto& all = this->available_drivers();

   if (  this->_selected_drivers.empty()
      )
   {
      std::vector<driver_t> drvs;
      drvs.reserve(all.size());

      for(const auto& d : all)
      {
         drvs.emplace_back(d.second);
      }

      return drvs;
   }
   else
   {
      std::vector<driver_t> drvs;
      drvs.reserve(this->_selected_drivers.size());

      for(const auto& k : this->_selected_drivers)
      {
         drvs.emplace_back(all.at(k));
      }

      return drvs;
   }
}

// Select driver
void driver_wrapper::select_driver
   (  const key_t& d
   )
{
   if (  this->is_valid_driver(d)
      )
   {
      this->_selected_drivers.emplace_back(this->parse(d));
   }
}

// Parse argument (transform to upper case)
typename driver_wrapper::key_t driver_wrapper::parse
   (  const key_t& k
   )  const
{
   auto ret = k;
   std::transform(ret.begin(), ret.end(), ret.begin(), [](unsigned char c){return std::tolower(c);});
   return ret;
}

// Check key
bool driver_wrapper::is_valid_driver
   (  const key_t& k
   )  const
{
   const auto& all = this->available_drivers();

   return (all.find(k) != all.end());
}


} /* namespace tulib::test */
