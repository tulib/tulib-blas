#ifndef LINALG_FWD_HPP_INCLUDED
#define LINALG_FWD_HPP_INCLUDED

namespace tulib::linalg
{

template
   <  typename T
   ,  typename SIZE_T = long
   >
class vector;

template
   <  typename T
   ,  typename SIZE_T = long
   >
class matrix;

} /* namespace tulib::linalg */

#endif /* LINALG_FWD_HPP_INCLUDED */
