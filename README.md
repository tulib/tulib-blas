BLAS extension for the TULib library including matrix/vector classes and LAPACK interface.

NB: This repository contains only a collection of files that have been moved from the tulib/tulib repository. A separate build and test system has not been set up yet.
