#ifndef LINALG_MATRIX_HPP_INCLUDED
#define LINALG_MATRIX_HPP_INCLUDED

#include <tulib/meta/complex.hpp>
#include <tulib/util/exceptions.hpp>
#include <tulib/linalg/array_base.hpp>
#include <tulib/linalg/linalg_fwd.hpp>
#include <tulib/linalg/assert_same_shape_fwd.hpp>
#include <tulib/linalg/lapack_wrapper/lapack_wrapper.hpp>

namespace tulib
{

/**
 * Matrix class
 *
 * Data is column major to interface with LAPACK
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
class matrix
   :  public linalg::detail::array_base<T, SIZE_T>
{
   public:
      //!@{
      using base_type = linalg::detail::array_base<T, SIZE_T>;
      using param_type = typename base_type::param_type;
      using real_type = typename base_type::real_type;
      using size_type = typename base_type::size_type;
      //!@}

      //! Default c-tor
      matrix() = default;

      //! c-tor
      matrix
         (  size_type a_nrow
         ,  size_type a_ncol
         ,  param_type a_val = 0
         )
         :  base_type
               (  a_nrow * a_ncol
               )
         ,  _nrow
               (  a_nrow
               )
         ,  _ncol
               (  a_ncol
               )
      {
         auto size = a_nrow*a_ncol;
         auto* ptr = this->get_data();
         for(size_type i=0; i<size; ++i)
         {
            ptr[i] = a_val;
         }
      }

      //! Copy c-tor
      matrix
         (  const matrix& a_other
         )
         :  base_type
               (  a_other._nrow * a_other._ncol
               )
         ,  _nrow
               (  a_other._nrow
               )
         ,  _ncol
               (  a_other._ncol
               )
      {
         // Reserve only does something if size > _capacity
         auto size = this->_nrow * this->_ncol;
         this->reserve(size);

         auto* ptr = this->get_data();
         const auto* other_ptr = a_other.get_data();

         for(size_type i=0; i<size; ++i)
         {
            ptr[i] = other_ptr[i];
         }
      }

      //! Move c-tor
      matrix
         (  matrix&& a_other
         )  noexcept
         :  base_type
               (  std::move(a_other)
               )
         ,  _nrow
               (  std::move(a_other._nrow)
               )
         ,  _ncol
               (  std::move(a_other._ncol)
               )
      {
      }

      //! Copy assign
      matrix& operator=
         (  const matrix& a_other
         )
      {
         if (  this != &a_other
            )
         {
            this->_nrow = a_other._nrow;
            this->_ncol = a_other._ncol;
            auto size = this->_nrow * this->_ncol;
            this->reserve(size);

            auto* ptr = this->get_data();
            const auto* other_ptr = a_other.get_data();

            for(size_type i=0; i<size; ++i)
            {
               ptr[i] = other_ptr[i];
            }
         }

         return *this;
      }

      //! Move assign
      matrix& operator=
         (  matrix&& a_other
         )  noexcept
      {
         base_type::operator=(std::move(a_other));
         this->_nrow = std::move(a_other._nrow);
         this->_ncol = std::move(a_other._ncol);

         return *this;
      }

      //! Resize
      void resize
         (  size_type a_nrow
         ,  size_type a_ncol
         ,  bool a_save_old = true
         )
      {
         auto new_size = a_nrow * a_ncol;

         // If same size, do nothing
         if (  a_nrow == this->_nrow
            && a_ncol == this->_ncol
            )
         {
            return;
         }
         // If same number of rows (or not save old data), just reserve more memory
         else if  (  a_nrow == this->_nrow
                  || !a_save_old
                  )
         {
            this->reserve(new_size, a_save_old);
         }
         // Else, we need to move data around
         else
         {
            if (  a_nrow > this->_nrow
               )
            {
               this->reserve(new_size, a_save_old);

               auto old_index = [this](size_type i, size_type j) { return i + this->_nrow*j; };
               auto new_index = [&](size_type i, size_type j)    { return i + a_nrow*j; };

               for(size_type icol=this->_ncol-1; icol>=0; --icol)
               {
                  for(size_type irow=this->_nrow-1; irow>=0; --irow)
                  {
                     this->get_data()[new_index(irow, icol)] = this->get_data()[old_index(irow, icol)];
                  }
               }
            }
            else
            {
               TULIB_EXCEPTION("Not implemented yet!"); 
            }
         }

         // Finally, set new sizes
         this->_nrow = a_nrow;
         this->_ncol = a_ncol;
      }

      //! Get dimensions
      size_type nrow
         (
         )  const noexcept
      {
         return this->_nrow;
      }
      size_type ncol
         (
         )  const noexcept
      {
         return this->_ncol;
      }

      //! Is square?
      bool is_square
         (
         )  const noexcept
      {
         return this->_nrow == this->_ncol;
      }

      /** @name Element access **/
      //!@{
      param_type& operator()
         (  size_type a_row_idx
         ,  size_type a_col_idx
         )  noexcept
      {
         return this->get_data()[a_row_idx + this->_nrow*a_col_idx];
      }
      const param_type& operator()
         (  size_type a_row_idx
         ,  size_type a_col_idx
         )  const noexcept
      {
         return this->get_data()[a_row_idx + this->_nrow*a_col_idx];
      }
      param_type& at
         (  size_type a_row_idx
         ,  size_type a_col_idx
         )
      {
         if (  a_row_idx < this->_nrow
            && a_col_idx < this->_ncol
            )
         {
            return this->get_data()[a_row_idx + this->_nrow*a_col_idx];
         }
         else
         {
            throw std::out_of_range("Indices out of range!");
         }
      }
      const param_type& at
         (  size_type a_row_idx
         ,  size_type a_col_idx
         )  const
      {
         if (  a_row_idx < this->_nrow
            && a_col_idx < this->_ncol
            )
         {
            return this->get_data()[a_row_idx + this->_nrow*a_col_idx];
         }
         else
         {
            throw std::out_of_range("Indices out of range!");
         }
      }
      //!@}

      /** @name Column access **/
      //!@{
      param_type* colptr
         (  size_type a_col_idx
         )
      {
         return this->get_data() + this->_nrow*a_col_idx;
      }
      const param_type* colptr
         (  size_type a_col_idx
         )  const
      {
         return this->get_data() + this->_nrow*a_col_idx;
      }
      //!@}

      /** @name Math interface **/
      //!@{
      matrix& operator+=
         (  const matrix& a_other
         )
      {
         assert_same_shape(*this, a_other);
         auto size = this->size();
         auto* ptr = this->get_data();
         const auto* other_ptr = a_other.get_data();
         for(size_type i=0; i<size; ++i)
         {
            ptr[i] += other_ptr[i];
         }
         return *this;
      }

      matrix& operator-=
         (  const matrix& a_other
         )
      {
         assert_same_shape(*this, a_other);
         auto size = this->size();
         auto* ptr = this->get_data();
         const auto* other_ptr = a_other.get_data();
         for(size_type i=0; i<size; ++i)
         {
            ptr[i] -= other_ptr[i];
         }
         return *this;
      }

      template
         <  typename SCALAR
         >
      matrix& scale
         (  const SCALAR& a_scalar
         )
      {
         auto size = this->size();
         auto* ptr = this->get_data();
         for(size_type i=0; i<size; ++i)
         {
            ptr[i] *= a_scalar;
         }
         return *this;
      }

      template
         <  typename SCALAR
         >
      matrix& axpy
         (  const SCALAR& a_scalar
         ,  const matrix& a_other
         )
      {
         assert_same_shape(*this, a_other);
         int size = (int)this->size();
         T* ptr = this->get_data();
         T* other_ptr = const_cast<matrix&>(a_other).get_data();
         auto a = T(a_scalar);
         int inc = 1;

         lapack_wrapper::axpy(&size, &a, other_ptr, &inc, ptr, &inc);

         return *this;
      }
      //!@}

   private:
      //! Size
      size_type _nrow = 0;
      size_type _ncol = 0;

      //! Size impl
      size_type size_impl
         (
         )  const override
      {
         return this->_nrow * this->_ncol;
      }

      //! Clear impl
      void clear_impl
         (
         )  override
      {
         this->_nrow = 0;
         this->_ncol = 0;
      }
};

} /* namespace tulib */

#include <tulib/linalg/assert_same_shape.hpp>
#include <tulib/linalg/lapack_wrapper/blas_interface.hpp>

namespace tulib
{

//! Math operators
template
   <  typename T
   , typename SIZE_T = long
   >
matrix<T, SIZE_T> operator+
   (  const matrix<T,SIZE_T>& a_left
   ,  const matrix<T,SIZE_T>& a_right
   )
{
   auto result = a_left;
   result += a_right;
   return result;
}

template
   <  typename T
   ,  typename SIZE_T = long
   >
matrix<T,SIZE_T> operator-
   (  const matrix<T,SIZE_T>& a_left
   ,  const matrix<T,SIZE_T>& a_right
   )
{
   auto result = a_left;
   result -= a_right;
   return result;
}

/**
 * Output
 *
 * @param a_os
 * @param a_mat
 * @return
 *    a_os
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
std::ostream& operator<<
   (  std::ostream& a_os
   ,  const matrix<T, SIZE_T>& a_mat
   )
{
   using size_type = SIZE_T;

   for(size_type i=0; i<a_mat.nrow(); ++i)
   {
      for(size_type j=0; j<a_mat.ncol(); ++j)
      {
         a_os  << a_mat(i,j) << "  ";
      }
      a_os  << std::endl;
   }
   return a_os;
}

//! make random
template
   <  typename T
   ,  typename SIZE_T = long
   >
matrix<T, SIZE_T> make_random_matrix
   (  long a_nrows
   ,  long a_ncols
   ,  const std::function<T(SIZE_T, SIZE_T)>& f_ = [](SIZE_T i_, SIZE_T j_) -> T { return T(1.0); }
   )
{
   matrix<T, SIZE_T> result(a_nrows, a_ncols);

   for(long i=0; i<a_nrows; ++i)
   {
      for(long j=0; j<a_ncols; ++j)
      {
         result(i,j) = f_(i,j)*util::rand_signed_float<T>();
      }
   }

   return result;
}

//! make random
template
   <  typename T
   ,  typename SIZE_T = long
   >
matrix<T, SIZE_T> make_random_hermitian_matrix
   (  long a_size
   ,  const std::function<T(SIZE_T, SIZE_T)>& f_ = [](SIZE_T i_, SIZE_T j_) -> T { return T(1.0); }
   )
{
   matrix<T, SIZE_T> result(a_size, a_size);

   for(long i=0; i<a_size; ++i)
   {
      auto d = f_(i,i)*util::rand_signed_float<meta::real_type_t<T>>();
      result(i,i) = T(d);
      for(long j=i+1; j<a_size; ++j)
      {
         auto v = f_(i,j)*util::rand_signed_float<T>();
         result(i,j) = v;
         result(j,i) = math::conj(v);
      }
   }

   return result;
}

//! Make random Hermitian positive definite matrix by computing A^dagger A where A is random.
template
   <  typename T
   ,  typename SIZE_T = long
   >
matrix<T, SIZE_T> make_random_positive_definite_matrix
   (  long size_
   ,  const std::function<T(SIZE_T, SIZE_T)>& f_ = [](SIZE_T i_, SIZE_T j_) -> T { return T(1.0); }
   )
{
   auto rand = make_random_matrix<T, SIZE_T>(size_, size_, f_);
   matrix<T, SIZE_T> result(size_, size_);
   matrix_matrix_multiply(rand, rand, result, matrix_tag::general, meta::is_complex_v<T> ? 'C' : 'T', 'N');

   // Make sure it is Hermitian
   using size_type = SIZE_T;
   for(size_type i=0; i<size_; ++i)
   {
      if constexpr ( tulib::meta::is_complex_v<T> )
      {
         result(i,i) = T(result(i,i).real(), 0.0);
      }
      for(size_type j=i+1; j<size_; ++j)
      {
         result(j,i) = tulib::math::conj(result(i,j));
      }
   }

   return result;
}

} /* namespace tulib */

#endif /* LINALG_MATRIX_HPP_INCLUDED */
