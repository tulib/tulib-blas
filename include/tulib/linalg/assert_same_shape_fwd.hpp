#ifndef LINALG_ASSERT_SAME_SHAPE_FWD_HPP_INCLUDED
#define LINALG_ASSERT_SAME_SHAPE_FWD_HPP_INCLUDED

namespace tulib
{

template<typename T, typename SIZE_T> class matrix;
template<typename T, typename SIZE_T> class vector;

/**
 *
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
bool assert_same_shape
   (  const vector<T, SIZE_T>& a_left
   ,  const vector<T, SIZE_T>& a_right
   );

/**
 *
 **/
template
   <  typename T
   ,  typename SIZE_T
   >
bool assert_same_shape
   (  const matrix<T, SIZE_T>& a_left
   ,  const matrix<T, SIZE_T>& a_right
   );

} /* namespace tulib */

#endif /* LINALG_ASSERT_SAME_SHAPE_FWD_HPP_INCLUDED */
