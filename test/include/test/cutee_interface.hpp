#ifndef TULIB_CUTEE_INTERFACE_HPP_INCLUDED
#define TULIB_CUTEE_INTERFACE_HPP_INCLUDED

#include <cutee/cutee.hpp>

namespace tulib::test
{

void run_suite(cutee::suite&);

} /* namespace tulib::test */

#endif /* TULIB_CUTEE_INTERFACE_HPP_INCLUDED */
