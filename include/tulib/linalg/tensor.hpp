#ifndef LINALG_TENSOR_HPP_INCLUDED
#define LINALG_TENSOR_HPP_INCLUDED

#include <memory>

#include <tulib/linalg/tensor_base.hpp>

namespace tulib
{

/**
 * Wrapper for tensor classes
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
class tensor
{
   public:
      //! Alias
      using base_type = linalg::detail::tensor_base<T, SIZE_T>;
      using param_type = typename base_type::param_type;
      using real_type = typename base_type::real_type;
      using size_type = typename base_type::size_type;
      using dims_type = typename base_type::dims_type;

      //! Get tensor
      base_type* tensor
         (
         )
      {
         return this->_tensor.get();
      }
      const base_type* const tensor
         (
         )  const
      {
         return this->_tensor.get();
      }

   private:
      //! Pointer to tensor
      std::unique_ptr<base_type> _tensor = nullptr;
};

} /* namespace tulib */

#endif /* LINALG_TENSOR_HPP_INCLUDED */
