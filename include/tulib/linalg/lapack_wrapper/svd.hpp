#ifndef TULIB_SVD_HPP_INCLUDED
#define TULIB_SVD_HPP_INCLUDED

namespace tulib
{

/**
 * Wrapper for SVD solution
 **/
template
   <  typename T
   >
struct svd_holder
{
   using real_t = tulib::meta::real_type_t<T>;
   using size_type = std::size_t;
   template<typename U> using data_ptr = std::unique_ptr<U[]>;

   size_type _m = 0;
   size_type _n = 0;
   int _info = -1;

   data_ptr<T> _u = nullptr;
   data_ptr<T> _vt = nullptr;
   data_ptr<real_t> _s = nullptr;

   bool check
      (
      )  const
   {
      return (_info == 0) && _s;
   }

   size_type min
      (
      )  const
   {
      return std::min(_m, _n);
   }
};

/**
 * Types of SVD algorithms
 **/
struct svd_tag
{
   inline static struct standard_svd {} standard;
   inline static struct divide_and_conquer_svd {} divide_and_conquer;
};

/**
 * Do SVD (standard algorithm)
 **/
template
   <  typename T
   >
svd_holder<T> compute_svd
   (  T* a_matrix
   ,  size_t a_nrow
   ,  size_t a_ncol
   ,  typename svd_tag::standard_svd
   ,  char jobu = 'S'
   ,  char jobvt = 'S'
   )
{
   using real_t = tulib::meta::real_type_t<T>;

   int m = a_nrow;
   int n = a_ncol;
   int lda = std::max(1, m);
   auto s = std::make_unique<real_t[]>(std::min(m,n));
   int ldu = (jobu == 'A' || jobu == 'S') ? m : 1;
   int ucol = (jobu == 'O' || jobu == 'N') ? 0 : ((jobu == 'A') ? m : std::min(m,n));
   int usize = ldu*ucol;
   auto u = usize ? std::make_unique<T[]>(usize) : std::unique_ptr<T[]>(nullptr);
   int ldvt = (jobvt == 'O' || jobvt == 'N') ? 1 : ((jobvt == 'A' ? n : std::min(m,n)));
   int vsize = (jobvt == 'O' || jobvt == 'N') ? 0 : ldvt*n;
   auto vt = vsize ? std::make_unique<T[]>(vsize) : std::unique_ptr<T[]>(nullptr);
   int info;
   T work_query;
   int lwork = -1;

   // Interface is different for complex
   if constexpr   (  tulib::meta::is_complex_v<T>
                  )
   {
      // Workspace query
      lapack_wrapper::gesvd(&jobu, &jobvt, &m, &n, nullptr, &lda, nullptr, nullptr, &ldu, nullptr, &ldvt, &work_query, &lwork, nullptr, &info);
      if (  info != 0
         )
      {
         TULIB_EXCEPTION("GESVD workspace query. Info != 0.");
      }
      lwork = static_cast<int>(work_query.real());
      auto work = std::make_unique<T[]>(lwork);
      auto rwork = std::make_unique<real_t[]>(5*std::min(n,m));

      // Do calculation
      lapack_wrapper::gesvd(&jobu, &jobvt, &m, &n, a_matrix, &lda, s.get(), u.get(), &ldu, vt.get(), &ldvt, work.get(), &lwork, rwork.get(), &info);
   }
   else
   {
      // Workspace query
      lapack_wrapper::gesvd(&jobu, &jobvt, &m, &n, nullptr, &lda, nullptr, nullptr, &ldu, nullptr, &ldvt, &work_query, &lwork, &info);
      if (  info != 0
         )
      {
         TULIB_EXCEPTION("GESVD workspace query. Info != 0.");
      }
      lwork = static_cast<int>(work_query);
      auto work = std::make_unique<T[]>(lwork);

      // Do calculation
      lapack_wrapper::gesvd(&jobu, &jobvt, &m, &n, a_matrix, &lda, s.get(), u.get(), &ldu, vt.get(), &ldvt, work.get(), &lwork, &info);
   }

   // Move results
   svd_holder<T> result;
   result._s = std::move(s);
   result._u = std::move(u);
   result._vt = std::move(vt);
   result._info = info;
   result._m = m;
   result._n = n;

   return result;
}

/**
 * Do SVD (divide and conquer algorithm)
 **/
template
   <  typename T
   >
svd_holder<T> compute_svd
   (  T* a_matrix
   ,  size_t a_nrow
   ,  size_t a_ncol
   ,  typename svd_tag::divide_and_conquer_svd
   ,  char jobz = 'S'
   )
{
   using real_t = tulib::meta::real_type_t<T>;

   int m = a_nrow;
   int n = a_ncol;
   int lda = std::max(1, m);
   auto s = std::make_unique<real_t[]>(std::min(m,n));
   int ldu = (jobz == 'A' || jobz == 'S' || (jobz == 'O' && m < n)) ? m : 1;
   int ucol = ((jobz == 'O' && m >= n ) || jobz == 'N') ? 0 : ((jobz == 'A') ? m : std::min(m,n));
   int usize = ldu*ucol;
   auto u = usize ? std::make_unique<T[]>(usize) : std::unique_ptr<T[]>(nullptr);
   int ldvt = (jobz == 'O' || jobz == 'N') ? 1 : ((jobz == 'A' ? n : std::min(m,n)));
   int vsize = ((jobz == 'O' && m < n) || jobz == 'N') ? 0 : ldvt*n;
   auto vt = vsize ? std::make_unique<T[]>(vsize) : std::unique_ptr<T[]>(nullptr);
   int info;
   T work_query;
   int lwork = -1;
   auto iwork = std::make_unique<int[]>(8*std::min(m,n));
   // Interface is different for complex
   if constexpr   (  tulib::meta::is_complex_v<T>
                  )
   {
      // Workspace query
      lapack_wrapper::gesdd(&jobz, &m, &n, nullptr, &lda, nullptr, nullptr, &ldu, nullptr, &ldvt, &work_query, &lwork, nullptr, nullptr, &info);
      if (  info != 0
         )
      {
         TULIB_EXCEPTION("GESDD workspace query. Info != 0.");
      }
      lwork = static_cast<int>(work_query.real());
      auto work = std::make_unique<T[]>(lwork);
      int mx = std::max(m,n);
      int mn = std::min(m,n);
      int lrwork = (jobz == 'N') ? 5*mn : std::max(5*mn*mn + 5*mn, 2*mx*mn + 2*mn*mn + mn);
      auto rwork = std::make_unique<real_t[]>(lrwork);

      // Do calculation
      lapack_wrapper::gesdd(&jobz, &m, &n, a_matrix, &lda, s.get(), u.get(), &ldu, vt.get(), &ldvt, work.get(), &lwork, rwork.get(), iwork.get(), &info);
   }
   else
   {
      // Workspace query
      lapack_wrapper::gesdd(&jobz, &m, &n, nullptr, &lda, nullptr, nullptr, &ldu, nullptr, &ldvt, &work_query, &lwork, nullptr, &info);
      if (  info != 0
         )
      {
         TULIB_EXCEPTION("GESDD workspace query. Info != 0.");
      }
      lwork = static_cast<int>(work_query);
      auto work = std::make_unique<T[]>(lwork);

      // Do calculation
      lapack_wrapper::gesdd(&jobz, &m, &n, a_matrix, &lda, s.get(), u.get(), &ldu, vt.get(), &ldvt, work.get(), &lwork, iwork.get(), &info);
   }

   // Move results
   svd_holder<T> result;
   result._s = std::move(s);
   result._u = std::move(u);
   result._vt = std::move(vt);
   result._info = info;
   result._m = m;
   result._n = n;

   return result;
}

/**
 * Do SVD for matrix (standard algorithm)
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
svd_holder<T> compute_svd
   (  const matrix<T,SIZE_T>& a_matrix
   ,  typename svd_tag::standard_svd a_tag
   ,  char jobu = 'S'
   ,  char jobvt = 'S'
   )
{
   auto copy = a_matrix;
   return compute_svd(copy.get_data(), copy.nrow(), copy.ncol(), a_tag, jobu, jobvt);
}

/**
 * Do SVD for matrix (divide and conquer algorithm)
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
svd_holder<T> compute_svd
   (  const matrix<T,SIZE_T>& a_matrix
   ,  typename svd_tag::divide_and_conquer_svd a_tag
   ,  char jobz = 'S'
   )
{
   auto copy = a_matrix;
   return compute_svd(copy.get_data(), copy.nrow(), copy.ncol(), a_tag, jobz);
}

/**
 * Do SVD for matrix (default algorithm = divide and conquer)
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
svd_holder<T> compute_svd
   (  const matrix<T,SIZE_T>& a_matrix
   ,  char jobu = 'S'
   ,  char jobvt = 'S'
   )
{
   auto result = compute_svd(a_matrix, svd_tag::divide_and_conquer, jobu);

   // If GESDD does not converge, try GESVD
   if (  result._info > 0
      )
   {
      result = compute_svd(a_matrix, svd_tag::standard, jobu, jobvt);
   }

   return result;
}

/**
 * Compute condition number
 **/
template
   <  typename T
   ,  typename SIZE_T = long
   >
typename tulib::meta::real_type_t<T> condition_number
   (  const matrix<T,SIZE_T>& a_matrix
   )
{
   // Compute singular values only
   auto svd = compute_svd(a_matrix, 'N', 'N');

   return svd._s[0] / svd._s[svd.min()-1];
}

} /* namespace tulib */

#endif /* TULIB_SVD_HPP_INCLUDED */
