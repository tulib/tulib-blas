#ifndef TULIB_TEST_UTIL_TEST_HPP_INCLUDED
#define TULIB_TEST_UTIL_TEST_HPP_INCLUDED

#include <test/cutee_interface.hpp>

#include <vector>
#include <complex>
#include <map>
#include <type_traits>
#include <sstream>
#include <iostream>

#include <tulib/util/function_wrapper.hpp>
#include <tulib/util/input_definitions.hpp>
#include <tulib/util/io.hpp>

namespace tulib::test
{

namespace util::detail
{
//! Test class
class A
   :  private std::vector<double>
{
   public:
      double mf0() const
      {
         return 42.0;
      }

      int mf1(int i) const
      {
         return i;
      }

      std::complex<float> mf2(float r, float i) const noexcept
      {
         return std::complex<float>(r, i);
      }

      bool operator()(double d_) const { return d_ > 2.0; }
};

void f1(int& i)
{
   i=0;
}

enum class testKeyType : int
{  FIRST = 0
,  SECOND
,  THIRD
,  FOURTH
};

} /* namespace util::detail */

/**
 * Test the function_wrapper class
 **/
struct function_wrapper_test
   :  public cutee::test
{
   public:
      void run
         (
         )  override
      {
         // Lambda function as reference
         auto lambda = [](int i) { return i+2; };
         auto wrapped_lambda_ref = tulib::util::wrap_function(lambda);
         static_assert(std::is_same_v<decltype(wrapped_lambda_ref), tulib::util::detail::function_wrapper_impl<decltype(lambda), true>>, "lambda is not wrapped as reference");
         UNIT_ASSERT_EQUAL(wrapped_lambda_ref(2), 4, "wrapped_lambda_ref returns wrong result");

         // Lambda function as value
         auto wrapped_lambda_val = tulib::util::wrap_function([](double x) -> double { return 2.0*x; });
         static_assert(std::is_same_v<decltype(wrapped_lambda_val), tulib::util::detail::function_wrapper_impl<typename decltype(wrapped_lambda_val)::function_type, false>>, "lambda is not wrapped as value");
         UNIT_ASSERT_FEQUAL(wrapped_lambda_val(3.0), 6.0, "wrapped_lambda_val returns wrong result");

         // Function pointer
         auto wrapped_fptr = tulib::util::wrap_function(&util::detail::f1);
         static_assert(std::is_same_v<decltype(wrapped_fptr), tulib::util::detail::function_wrapper_impl<decltype(&util::detail::f1), false>>, "f1 is not wrapped as value");
         int i = 1;
         wrapped_fptr(i);
         UNIT_ASSERT_EQUAL(i, 0, "wrapped_fptr returns wrong result");

         // Functor as reference
         util::detail::A a;
         auto wrapped_functor_ref = tulib::util::wrap_function(a);
         static_assert(std::is_same_v<decltype(wrapped_functor_ref), tulib::util::detail::function_wrapper_impl<util::detail::A, true>>, "functor is not wrapped as reference");
         UNIT_ASSERT(wrapped_functor_ref(2.5), "wrapped_functor_ref returns wrong result");

         // Move instead
         auto wrapped_functor_val = tulib::util::wrap_function(std::move(a));
         static_assert(std::is_same_v<decltype(wrapped_functor_val), tulib::util::detail::function_wrapper_impl<util::detail::A, false>>, "functor is not wrapped as value");
         UNIT_ASSERT(wrapped_functor_val(2.5), "wrapped_functor_val returns wrong result");
      }
};

/**
 * Test input_definitions class
 **/
struct input_definitions_test
   :  public cutee::test
{
   public:
      void run
         (
         )  override
      {
         tulib::util::input_definitions<util::detail::testKeyType> def;
         using map_type = typename decltype(def)::map_type;
         using defaults_map_type = typename decltype(def)::defaults_map_type<std::string>;

         // Add and get values
         def.add(util::detail::testKeyType::FIRST, 4.5);
         def.add(util::detail::testKeyType::SECOND, 1);
         def.add(util::detail::testKeyType::THIRD, std::string("third"));
         auto x1f = def.get<float>(util::detail::testKeyType::FIRST);
         static_assert(std::is_same_v<decltype(x1f), float>, "Expected float");
         UNIT_ASSERT_FEQUAL(x1f, 4.5f, "got wrong float");
         auto x1d = def.get<double>(util::detail::testKeyType::FIRST);
         static_assert(std::is_same_v<decltype(x1d), double>, "Expected double");
         UNIT_ASSERT_FEQUAL(x1d, 4.5, "got wrong double");
         auto x2 = def.get<int>(util::detail::testKeyType::SECOND);
         UNIT_ASSERT_EQUAL(x2, 1, "got wrong int");
         auto x3 = def.get<std::string>(util::detail::testKeyType::THIRD);
         UNIT_ASSERT_EQUAL(x3, "third", "got wrong string");

         // Try validation
         typename decltype(def)::defaults_map_type<std::string> defaults;
         defaults[util::detail::testKeyType::THIRD] = 
         {  {  "third"
            ,  {  {util::detail::testKeyType::FIRST, 2.3}
               ,  {util::detail::testKeyType::FOURTH, 7}
               }
            }
         ,  {  "third2"
            ,  {  {util::detail::testKeyType::SECOND, 9}
               ,  {util::detail::testKeyType::FOURTH, 3}
               }
            }
         };
         UNIT_ASSERT(def.validate(defaults), "Validation failed");
         UNIT_ASSERT_EQUAL(def.get<int>(util::detail::testKeyType::FOURTH), 7, "Wrong default set for FOURTH");
         UNIT_ASSERT_EQUAL(def.get<double>(util::detail::testKeyType::FIRST), 4.5, "Wrong value of FIRST after validation");
      }
};

/**
 * Test io methods and classes
 **/
struct io_test
   :  public cutee::test
{
   public:
      void run
         (
         )  override
      {
         std::ostringstream oss5, oss8;
         tulib::io::writer_collection wc({{oss5, 5}, {oss8, 8}});

         wc << "Test1";
         UNIT_ASSERT_EQUAL(oss5.str(), "Test1", "could not write correctly to ostringstream");
         UNIT_ASSERT_EQUAL(oss8.str(), "Test1", "could not write correctly to ostringstream");
         {
            tulib::io::SCOPE_IOLEVEL(wc, 6);
            wc << "Test2";
         }
         UNIT_ASSERT_EQUAL(oss5.str(), "Test1", "scoped iolevel failed");
         UNIT_ASSERT_EQUAL(oss8.str(), "Test1Test2", "scoped iolevel failed");
         wc << "Test3";
         UNIT_ASSERT_EQUAL(oss5.str(), "Test1Test3", "scoped iolevel failed to reset");
         UNIT_ASSERT_EQUAL(oss8.str(), "Test1Test2Test3", "scoped iolevel failed to reset");

         std::ostringstream oss;
         tulib::io::writer_collection wc2(oss, 2);
         {
            tulib::io::SCOPE_INDENT(wc2);
            wc2 << "Test";
         }
         UNIT_ASSERT_EQUAL(oss.str(), "   Test", "scoped indentation failed");
         wc2 << "Test2";
         UNIT_ASSERT_EQUAL(oss.str(), "   TestTest2", "scoped indentation failed to reset");
      }
};


} /* namespace tulib::test */

#endif /* TULIB_TEST_UTIL_TEST_HPP_INCLUDED */

