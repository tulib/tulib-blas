#include <tulib/util/random.hpp>

namespace tulib::util
{
namespace detail
{

// mersenne twister
std::mt19937 mersenne_twister;
bool mersenne_twister_init = false;

// Getter for mersenne twister
std::mt19937& get_mersenne_twister()
{
   if (  !mersenne_twister_init
      )
   {
      seed_rand();
   }
   return mersenne_twister;
}

} /* namespace detail */


/**
 * Seed random number generator.
 *
 * @param seed_vec   Seed for initialization.
 **/
void seed_rand
   (  std::vector<unsigned int> seed_vec
   )
{
   if (  !detail::mersenne_twister_init
      )
   {
      if (  seed_vec.empty()
         )
      {
         // Seed by random device
         std::random_device r;
         seed_vec = std::vector<unsigned int>({r(), r(), r(), r(), r(), r(), r(), r()});
      }
      
      std::seed_seq seed(seed_vec.begin(), seed_vec.end());
      detail::mersenne_twister.seed(seed);

      detail::mersenne_twister_init = true;
   }
}

} /* namespace tulib::util */
