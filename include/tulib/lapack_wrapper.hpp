#ifndef TULIB_LAPACK_WRAPPER_INCLUDED
#define TULIB_LAPACK_WRAPPER_INCLUDED

#include <tulib/linalg/lapack_wrapper/lapack_wrapper.hpp>
#include <tulib/linalg/lapack_wrapper/blas_interface.hpp>
#include <tulib/linalg/lapack_wrapper/linear_system.hpp>
#include <tulib/linalg/lapack_wrapper/tags.hpp>
#include <tulib/linalg/lapack_wrapper/svd.hpp>

#endif /* TULIB_LAPACK_WRAPPER_INCLUDED */
